defmodule Store.MixProject do
  use Mix.Project

  @project_url "https://gitlab.com/bobwaycott/store"

  def project do
    [
      app: :store,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),

      # Dialyzer
      dialyzer: [
        plt_add_deps: :apps_direct,
        plt_add_apps: [:mix],
        list_unused_filters: true
      ],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.details": :test,
        "coveralls.html": :test
      ],

      # Hex.pm
      description: description(),
      package: package(),

      # Docs
      name: "Store",
      source_url: @project_url,
      homepage_url: @project_url,
      docs: [main: "Store", extras: ["README.md"]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :runtime_tools, :postgrex, :ecto],
      mod: {Store.Application, []}
    ]
  end

  # Command aliases
  defp aliases do
    [
      "store.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "store.reset": ["ecto.drop", "store.setup"],
      "test.setup": [
        "ecto.drop --quiet",
        "ecto.create --quiet",
        "ecto.migrate --quiet",
        "run priv/repo/test_seeds.exs --quiet"
      ],
      test: ["test.setup", "test"]
    ]
  end

  defp description do
    """
    A simple, drop-in store powered by Stripe.
    """
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:assert_value, ">= 0.0.0", only: [:dev, :test]},
      {:dialyxir, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:ecto_sql, ">= 0.0.0"},
      {:excoveralls, ">= 0.0.0", only: :test},
      {:exexec, ">= 0.0.0", only: :test},
      {:ex_cldr, ">= 0.0.0"},
      {:ex_cldr_numbers, ">= 0.0.0"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:ex_money, ">= 0.0.0"},
      {:faker, ">= 0.0.0", only: [:dev, :test]},
      {:inch_ex, ">= 0.0.0", only: [:dev, :test]},
      {:jason, ">= 0.0.0"},
      {:mox, ">= 0.0.0", only: :test},
      {:postgrex, ">= 0.0.0"},
      {:stream_data, ">= 0.0.0", only: [:dev, :test]},
      {:stripity_stripe, "~> 2.3.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/fixtures"]
  defp elixirc_paths(:dev), do: ["lib", "test/fixtures"]
  defp elixirc_paths(_), do: ["lib"]

  defp package do
    [
      files: ["lib", "LICENSE*", "mix.exs", "README*"],
      licenses: ["New BSD"],
      links: %{"Gitlab" => @project_url},
      maintainers: ["Bob Waycott"]
    ]
  end
end
