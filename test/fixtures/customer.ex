defmodule Store.CustomerFixture do
  @moduledoc """
  Provides simple customer fixtures for development and testing.

  Should not be used in production, obviously.
  """
  @required [:tos, :email, :name, :source]

  @doc """
  Returns an example create payload as a string for documentation.
  """
  @spec create_payload() :: String.t()
  def create_payload do
    """
    %{
      tos: true,
      email: "customer@example.org",
      name: "Happy Example Customer",
      source: "tok_visa"
    }
    """
  end

  @doc """
  Return a valid entity for tests.
  """
  @spec entity() :: map
  def entity do
    %Stripe.Customer{
      account_balance: 380,
      created: 1_550_368_288,
      currency: "usd",
      default_source: "card_1a2b3c4d5e6f7g8h9i10j11k",
      deleted: nil,
      delinquent: false,
      description: "Skyla Olson DDS",
      discount: nil,
      email: "ariel2100@leffler.name",
      id: "cus_1a2b3c4d5e6f7g",
      invoice_prefix: "43F6CF0",
      invoice_settings: %{custom_fields: nil, footer: nil},
      livemode: false,
      metadata: %{"baz" => "true", "foo" => "bar"},
      object: "customer",
      shipping: %{
        address: %{
          city: "Casper",
          country: "USA",
          line1: "8672 Golden Avenue",
          line2: "Apt. 783",
          postal_code: "36447",
          state: "Alaska"
        },
        name: "Skyla Olson DDS",
        phone: "(745) 205-7315"
      },
      sources: %Stripe.List{
        data: [
          %Stripe.Card{
            account: nil,
            address_city: nil,
            address_country: nil,
            address_line1: nil,
            address_line1_check: nil,
            address_line2: nil,
            address_state: nil,
            address_zip: nil,
            address_zip_check: nil,
            available_payout_methods: nil,
            brand: "MasterCard",
            country: "US",
            currency: nil,
            customer: "cus_1a2b3c4d5e6f7g",
            cvc_check: nil,
            default_for_currency: nil,
            deleted: nil,
            dynamic_last4: nil,
            exp_month: 2,
            exp_year: 2020,
            fingerprint: "75IKRLa6U6xW5NxE",
            funding: "debit",
            id: "card_1a2b3c4d5e6f7g8h9i10j11k",
            last4: "8210",
            metadata: %{},
            name: nil,
            object: "card",
            recipient: nil,
            tokenization_method: nil
          }
        ],
        has_more: false,
        object: "list",
        total_count: 1,
        url: "/v1/customers/cus_1a2b3c4d5e6f7g/sources"
      },
      subscriptions: %Stripe.List{
        data: [],
        has_more: false,
        object: "list",
        total_count: 0,
        url: "/v1/customers/cus_1a2b3c4d5e6f7g/subscriptions"
      },
      tax_info: nil,
      tax_info_verification: nil
    }
  end

  @doc """
  Returns a valid payload that would result in `entity`.
  """
  @spec entity_payload() :: map
  def entity_payload do
    %{
      account_balance: 380,
      description: "Skyla Olson DDS",
      email: "ariel2100@leffler.name",
      invoice_prefix: "43F6CF0",
      invoice_settings: %{custom_fields: nil, footer: nil},
      metadata: %{"baz" => "true", "foo" => "bar"},
      shipping: %{
        address: %{
          city: "Casper",
          country: "USA",
          line1: "8672 Golden Avenue",
          line2: "Apt. 783",
          postal_code: "36447",
          state: "Alaska"
        },
        name: "Skyla Olson DDS",
        phone: "(745) 205-7315"
      },
      source: "tok_1a2b3c4d5e6f7g8h9i10j11k",
      tos: true
    }
  end

  @doc """
  Full data map supplying all available attrs for create/update
  """
  @spec full_payload() :: map
  def full_payload do
    :rand.seed(:default, :os.timestamp())
    name = Faker.Person.name()

    %{
      account_balance: :rand.uniform(500),
      # coupon: :string,
      # default_source: :string,
      description: name,
      email: Faker.Internet.email(),
      metadata: %{"baz" => "true", "foo" => "bar"},
      shipping: %{
        address: %{
          city: Faker.Address.city(),
          country: "USA",
          line1: Faker.Address.street_address(),
          line2: Faker.Address.secondary_address(),
          postal_code: Faker.Address.zip(),
          state: Faker.Address.state()
        },
        name: name,
        phone: Faker.Phone.EnUs.phone()
      },
      source: Store.Token.random(),
      tax_info: nil,
      tos: true
    }
  end

  @doc """
  Known data for tests **only** (will not exist in Stripe).

  This is used to seed data & test `Store.Customer.email_available?/1` does its job.
  """
  @spec known() :: map
  def known do
    %{
      id: 1,
      email: "taken@example.org",
      description: "Happy Example Customer",
      stripe_id: "cus_1a2b3c4d5e6f7g",
      stripe_source: "card_1a2b3c4d5e6f7g8h9i10j11k",
      tos: true
    }
  end

  @doc """
  Data that should always be rejected for providing an improper `email` value.
  """
  @spec invalid_email() :: map
  def invalid_email do
    email =
      [
        String.replace(Faker.Internet.email(), "@", ""),
        String.replace(Faker.Internet.email(), ".", "")
      ]
      |> Enum.take_random(1)
      |> List.first()

    %{valid_payload() | email: email}
  end

  @doc """
  Empty data that should never get past validation.
  """
  @spec invalid_payload() :: map
  def invalid_payload do
    %{
      tos: true,
      email: nil,
      description: nil,
      source: nil
    }
    |> Map.take(Enum.take_random(@required, 2))
  end

  @doc """
  Data that should always be rejected for providing an invalid/missing `email` value.
  """
  @spec no_email() :: map
  def no_email do
    email =
      [
        nil,
        ""
      ]
      |> Enum.take_random(1)
      |> List.first()

    %{valid_payload() | email: email}
  end

  @doc """
  Data that should always be rejected for providing an invalid/missing `name` value.
  """
  @spec no_name() :: map
  def no_name do
    name =
      [nil, ""]
      |> Enum.take_random(1)
      |> List.first()

    %{valid_payload() | description: name}
  end

  @doc """
  Data that should always be rejected for neglecting to gain TOS agreement.
  """
  @spec no_tos() :: map
  def no_tos do
    %{valid_payload() | tos: false}
  end

  @doc """
  Data that should always be rejected for providing an invalid/missing `Stripe.Source`.
  """
  @spec no_source() :: map
  def no_source do
    source =
      [nil, ""]
      |> Enum.take_random(1)
      |> List.first()

    %{valid_payload() | source: source}
  end

  @doc """
  Fake data for seeded inserts (will not exist in Stripe)
  """
  @spec seed() :: map
  def seed do
    %{
      email: Faker.Internet.email(),
      description: Faker.Person.name(),
      stripe_id: "cus_" <> Faker.String.base64(14),
      stripe_source: "card_" <> Faker.String.base64(24),
      tos: true
    }
  end

  @doc """
  Returns an example update payload as a string for documentation.
  """
  @spec update_payload() :: String.t()
  def update_payload do
    """
    %{
      email: "customer@example.org",
      name: "Happiest Example Customer",
      source: "tok_mastercard"
    }
    """
  end

  @doc """
  Data that should always be accepted.
  """
  @spec valid_payload() :: map
  def valid_payload() do
    %{
      tos: true,
      email: Faker.Internet.email(),
      description: Faker.Person.name(),
      source: Store.Token.random()
    }
  end
end
