defmodule Store.ProductFixture do
  @moduledoc """
  Provides simple product fixtures for development and testing.

  Should not be used in production, obviously.
  """

  @doc """
  Returns an example create payload as a string for documentation.
  """
  @spec create_payload() :: String.t()
  def create_payload do
    """
    %{
      caption: "Goodbye ads. Hello privacy.",
      description: "Block ads, tracking, and more. For your whole network. On every device.",
      images: [
        "https://placehold.it/347x347",
        "https://placehold.it/218x218",
        "https://dummyimage.com/129x129",
        "https://dummyimage.com/798x798",
        "https://dummyimage.com/775x775"
      ],
      name: "Example v1"
    }
    """
  end

  @doc """
  Returns a valid entity for tests.
  """
  @spec entity() :: map
  def entity do
    %Stripe.Product{
      active: true,
      attributes: ["size", "material"],
      caption: "Nihil hic sapiente dolore qui voluptas iusto ad dolor ut?",
      created: 1_550_132_275,
      deactivate_on: [],
      deleted: nil,
      description:
        "Eveniet error totam ea ut quo quam quo vel. Voluptas et libero sunt? Rerum et inventore quas et culpa illum est. Non quibusdam eveniet temporibus accusamus atque.",
      id: "prod_1a2b3c4d5e6f7g",
      images: [
        "https://placehold.it/347x347",
        "https://placehold.it/218x218",
        "https://dummyimage.com/129x129",
        "https://dummyimage.com/798x798",
        "https://dummyimage.com/775x775"
      ],
      livemode: false,
      metadata: %{"industry" => "Technology", "number" => "385", "word" => "ut"},
      name: "Sleek Table",
      object: "product",
      package_dimensions: %{height: 7.0, length: 6.0, weight: 13.0, width: 9.0},
      shippable: true,
      statement_descriptor: nil,
      type: "good",
      unit_label: nil,
      updated: 1_550_132_275,
      url: "https://dach.com"
    }
  end

  @doc """
  Returns a valid payload t hat would result in `entity`.
  """
  @spec entity_payload() :: map
  def entity_payload do
    %{
      attributes: ["size", "material"],
      caption: "Nihil hic sapiente dolore qui voluptas iusto ad dolor ut?",
      description:
        "Eveniet error totam ea ut quo quam quo vel. Voluptas et libero sunt? Rerum et inventore quas et culpa illum est. Non quibusdam eveniet temporibus accusamus atque.",
      id: "prod_EWirr5T5pGs22T",
      images: [
        "https://placehold.it/347x347",
        "https://placehold.it/218x218",
        "https://dummyimage.com/129x129",
        "https://dummyimage.com/798x798",
        "https://dummyimage.com/775x775"
      ],
      metadata: %{"industry" => "Technology", "number" => "385", "word" => "ut"},
      name: "Sleek Table",
      package_dimensions: %{height: 7.0, length: 6.0, weight: 13.0, width: 9.0},
      shippable: true,
      type: "good",
      url: "https://dach.com"
    }
  end

  @doc """
  Full data map supplying all available attrs for create/update
  """
  @spec full_payload() :: map
  def full_payload do
    :rand.seed(:default, :os.timestamp())

    %{
      attributes: ["size", "material"],
      caption: Faker.Lorem.sentence(5..10),
      # deactivate_on: :list,
      description: Enum.join(Faker.Lorem.sentences(2..5), " "),
      images: for(_ <- 1..:rand.uniform(8), do: Faker.Internet.image_url()),
      name: Faker.Commerce.product_name(),
      metadata: %{
        word: Faker.Lorem.word(),
        number: to_string(:rand.uniform(500)),
        industry: Faker.Industry.industry()
      },
      package_dimensions: %{
        height: :rand.uniform(10),
        length: :rand.uniform(10),
        weight: :rand.uniform(24),
        width: :rand.uniform(10)
      },
      shippable: Enum.take_random([true, false], 1) |> List.first(),
      url: Faker.Internet.url()
    }
  end

  @doc """
  Known data for tests **only** (will not exist in Stripe).
  """
  @spec known() :: map
  def known do
    %{
      attributes: ["size", "material"],
      caption: "Nihil hic sapiente dolore qui voluptas iusto ad dolor ut?",
      description:
        "Eveniet error totam ea ut quo quam quo vel. Voluptas et libero sunt? Rerum et inventore quas et culpa illum est. Non quibusdam eveniet temporibus accusamus atque.",
      id: 1,
      images: [
        "https://placehold.it/347x347",
        "https://placehold.it/218x218",
        "https://dummyimage.com/129x129",
        "https://dummyimage.com/798x798",
        "https://dummyimage.com/775x775"
      ],
      name: "Sleek Table",
      stripe_id: "prod_1a2b3c4d5e6f7g"
    }
  end

  @doc """
  Invalid payload with too many attributes.
  """
  @spec invalid_attributes(integer) :: map
  def invalid_attributes(count) do
    %{
      attributes: for(_ <- 1..count, do: Faker.Lorem.word()),
      name: Faker.Commerce.product_name()
    }
  end

  @doc """
  Invalid payload with too many images.
  """
  @spec invalid_images(integer) :: map
  def invalid_images(count) do
    %{
      images: for(_ <- 1..count, do: Faker.Internet.image_url()),
      name: Faker.Commerce.product_name()
    }
  end

  @doc """
  Empty data that should never get past validation.
  """
  @spec invalid_payload() :: map
  def invalid_payload do
    %{}
  end

  @doc """
  Invalid package dimensions map missing height.
  """
  @spec no_height() :: map
  def no_height() do
    %{
      name: Faker.Commerce.product_name(),
      package_dimensions: %{
        length: :rand.uniform(10),
        weight: :rand.uniform(24),
        width: :rand.uniform(10)
      }
    }
  end

  @doc """
  Invalid package dimensions map missing length.
  """
  @spec no_length() :: map
  def no_length() do
    %{
      name: Faker.Commerce.product_name(),
      package_dimensions: %{
        height: :rand.uniform(10),
        weight: :rand.uniform(24),
        width: :rand.uniform(10)
      }
    }
  end

  @doc """
  Invalid package dimensions map missing weight.
  """
  @spec no_weight() :: map
  def no_weight() do
    %{
      name: Faker.Commerce.product_name(),
      package_dimensions: %{
        height: :rand.uniform(10),
        length: :rand.uniform(10),
        width: :rand.uniform(10)
      }
    }
  end

  @doc """
  Invalid package dimensions map missing width.
  """
  @spec no_width() :: map
  def no_width() do
    %{
      name: Faker.Commerce.product_name(),
      package_dimensions: %{
        height: :rand.uniform(10),
        length: :rand.uniform(10),
        weight: :rand.uniform(24)
      }
    }
  end

  @doc """
  Fake data for seeded inserts (will not exist in Stripe)
  """
  @spec seed() :: map
  def seed do
    full_payload()
    |> Map.put(:stripe_id, "prod_" <> Faker.String.base64(14))
  end

  @doc """
  Returns an example update payload as a string for documentation.
  """
  @spec update_payload() :: String.t()
  def update_payload do
    """
    %{
      attributes: ["version", "color"]
      caption: "Goodbye malware. Hello safety.",
      description: "Block ads, tracking, malware, and more. For your whole network. On every device.",
      images: [
        "https://placehold.it/347x347",
        "https://placehold.it/218x218"
      ],
      name: "Example v2"
    }
    """
  end

  @doc """
  Data that should always be accepted.
  """
  @spec valid_payload() :: map
  def valid_payload() do
    %{
      name: Faker.Commerce.product_name()
    }
  end
end
