defmodule Store.OrderFixture do
  @moduledoc """
  Provides simple order fixtures for development and testing.

  Should not be used in production, obviously.
  """
  @required []

  @doc """
  Returns an example create payload as a string for documentation.
  """
  @spec create_payload() :: String.t()
  def create_payload do
    """
    %{
      ...
    }
    """
  end

  @doc """
  Fake data for seeded inserts (will not exist in Stripe)
  """
  @spec seed() :: map
  def seed do
    %{}
  end

  @doc """
  Full data map supplying all available attrs for create/update
  """
  @spec full_payload() :: map
  def full_payload do
    %{}
  end

  @doc """
  Known data for tests **only** (will not exist in Stripe).
  """
  @spec known() :: map
  def known do
    %{}
  end

  @doc """
  Empty data that should never get past validation.
  """
  @spec invalid_payload() :: map
  def invalid_payload do
    %{}
    |> Map.take(Enum.take_random(@required, 2))
  end

  @doc """
  Returns an example update payload as a string for documentation.
  """
  @spec update_payload() :: String.t()
  def update_payload do
    """
    %{
      ...
    }
    """
  end

  @doc """
  Data that should always be accepted.
  """
  @spec valid_payload() :: map
  def valid_payload() do
    %{}
  end
end
