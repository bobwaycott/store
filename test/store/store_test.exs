defmodule StoreTest do
  use Store.StoreCase

  test "change/1 accepts :customer module and returns changeset" do
    res =
      Store.change(:customer)
      |> inspect(limit: :infinity, printable_limit: :infinity)

    assert_value res == File.read!("test/results/Store.change:1:customer.txt")
  end

  test "change/1 accepts modules that aren't yet implemented and returns Error.not_implemented" do
    res =
      [:order, :product, :sku]
      |> Enum.take_random(1)
      |> List.first()
      |> Store.change()

    assert_value res == Error.not_implemented()
  end

  property "change/1 rejects invalid modules with Error.invalid_module" do
    check all mod <- StreamData.atom(:alphanumeric),
              mod not in [:customer, :order, :product, :sku] do
      assert_value Store.change(mod) == Error.invalid_module()
    end
  end
end
