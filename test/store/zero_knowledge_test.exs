defmodule Store.ZeroKnowledgeTest do
  use Store.StoreCase

  test "customer schema has no PII" do
    keys =
      %Store.Customer.Schema{}
      |> Map.from_struct()
      |> Map.keys()

    refute :email in keys
    refute :description in keys
  end
end
