defmodule ProductCreateTest do
  alias Store.Product.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.CreateCase, main: @main, data: @data

  # Verifies payload persisted correctly
  defp verify_record(data, product) do
    %{record: record, entity: _} = product
    refute is_nil(record)
    assert Map.get(data, :attributes, []) == record.attributes
    assert data[:caption] == record.caption
    assert data[:description] == record.description
    assert Map.get(data, :images, []) == record.images
    assert data[:name] == record.name
  end

  # Verifies record keys match their Stripe entity counterparts.
  defp verify_entity(product) do
    %{record: record, entity: entity} = product
    refute is_nil(entity)
    assert_value record.stripe_id == entity.id
    assert_value entity.attributes == record.attributes
    assert_value entity.caption == record.caption
    assert_value entity.description == record.description
    assert_value entity.images == record.images
    assert_value entity.name == record.name
  end
end
