defmodule ProductDeleteTest do
  alias Store.Product.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.DeleteCase, main: @main, data: @data
end
