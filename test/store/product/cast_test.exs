defmodule Store.ProductCastTest do
  use Store.StoreCase
  alias Store.Product.Meta

  @cast Meta.cast()
  @data Meta.data()
  @entity @data.entity()
  @payload @data.entity_payload()

  test "entity/1 returns Stripe entity keys only" do
    entity = @cast.entity(@payload)

    assert_value Map.keys(entity) == [
                   :attributes,
                   :caption,
                   :description,
                   :images,
                   :metadata,
                   :name,
                   :package_dimensions,
                   :shippable,
                   :type,
                   :url
                 ]
  end

  test "record_create/2 returns correctly remapped fields for database" do
    res = @cast.record_create(@entity, @payload)

    assert_value Map.keys(res) == [
                   :active,
                   :attributes,
                   :caption,
                   :description,
                   :images,
                   :name,
                   :stripe_id
                 ]

    assert_value @entity.id == res.stripe_id
  end

  test "record_update/1 returns correctly remapped fields for database" do
    res = @cast.record_update(@entity)

    assert_value Map.keys(res) == [
                   :active,
                   :attributes,
                   :caption,
                   :description,
                   :images,
                   :name,
                   :stripe_id
                 ]

    assert_value @entity.id == res.stripe_id
  end
end
