defmodule ProductReadTest do
  alias Store.Product.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.ReadCase, main: @main, data: @data
end
