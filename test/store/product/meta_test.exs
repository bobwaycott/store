defmodule Store.ProductMetaTest do
  alias Store.Product.Meta
  use Store.StoreCase

  test "has expected cast/0" do
    assert Meta.cast() == Store.Product.Cast
  end

  test "has expected create_payload/0 for docs" do
    payload = Meta.create_payload()
    assert is_binary(payload)

    assert_value(
      payload == """
      %{
        caption: \"Goodbye ads. Hello privacy.\",
        description: \"Block ads, tracking, and more. For your whole network. On every device.\",
        images: [
          \"https://placehold.it/347x347\",
          \"https://placehold.it/218x218\",
          \"https://dummyimage.com/129x129\",
          \"https://dummyimage.com/798x798\",
          \"https://dummyimage.com/775x775\"
        ],
        name: \"Example v1\"
      }
      """
    )
  end

  test "has expected data/0" do
    assert Meta.data() == Store.ProductFixture
  end

  test "data/0 is nil in live mode" do
    Mix.env(:prod)
    assert_value(Meta.data() == nil)
    Mix.env(:test)
  end

  test "has expected entity/0" do
    assert Meta.entity() == Stripe.Relay.Product
  end

  test "has expected lookups/0" do
    assert is_list(Meta.lookups())
    assert_value(Meta.lookups() == [:stripe_id])
  end

  test "has expected mod/0" do
    assert Meta.mod() == Store.Product
  end

  test "has expected record/0" do
    assert Meta.record() == Store.Product.Schema
  end

  test "has expected thing/0" do
    assert_value(Meta.thing() == "product")
  end

  test "has expected things/0" do
    assert_value(Meta.things() == "products")
  end

  test "has expected update_payload/0" do
    payload = Meta.update_payload()
    assert is_binary(payload)

    assert_value(
      payload == """
      %{
        attributes: [\"version\", \"color\"]
        caption: \"Goodbye malware. Hello safety.\",
        description: \"Block ads, tracking, malware, and more. For your whole network. On every device.\",
        images: [
          \"https://placehold.it/347x347\",
          \"https://placehold.it/218x218\"
        ],
        name: \"Example v2\"
      }
      """
    )
  end

  test "has expected validator/0" do
    assert Meta.validator() == Store.Product.Validator
  end
end
