defmodule ProductTest do
  use Store.StoreCase
  alias Store.Product.Meta
  alias Store.Schema.{Customer, Order, Sku}

  @main Meta.mod()
  @record Meta.record()

  test "change/1 accepts empty customer record and returns changeset" do
    res =
      @main.change(%@record{})
      |> inspect(limit: :infinity, printable_limit: :infinity)

    assert_value res == File.read!("test/results/Store.Product.change:1-empty.txt")
  end

  test "change/1 does not accept other record types" do
    res =
      Enum.take([Customer, Order, Sku], 1)
      |> @main.change()

    assert_value res == Error.invalid()
  end
end
