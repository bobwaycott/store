defmodule Store.ProductValidatorTest do
  alias Store.Product.Meta

  @data Meta.data()
  @validator Meta.validator()

  use Store.ValidatorCase, data: @data, validator: @validator

  property "rejects attributes longer than 5 items" do
    check all count <- StreamData.integer(6..20) do
      refute @validator.validate_create(@data.invalid_attributes(count)).valid?
      refute @validator.validate_update(@data.invalid_attributes(count)).valid?
    end
  end

  property "rejects images longer than 8 items" do
    check all count <- StreamData.integer(9..20) do
      refute @validator.validate_create(@data.invalid_images(count)).valid?
      refute @validator.validate_update(@data.invalid_images(count)).valid?
    end
  end

  test "rejects package dimensions without height" do
    refute @validator.validate_create(@data.no_height()).valid?
    refute @validator.validate_update(@data.no_height()).valid?
  end

  test "rejects package dimensions without length" do
    refute @validator.validate_create(@data.no_length()).valid?
    refute @validator.validate_update(@data.no_length()).valid?
  end

  test "rejects package dimensions without weight" do
    refute @validator.validate_create(@data.no_weight()).valid?
    refute @validator.validate_update(@data.no_weight()).valid?
  end

  test "rejects package dimensions without width" do
    refute @validator.validate_create(@data.no_width()).valid?
    refute @validator.validate_update(@data.no_width()).valid?
  end
end
