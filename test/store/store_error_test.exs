defmodule StoreErrorTest do
  use Store.StoreCase

  test "dev_mode_only/0 returns correct error tuple" do
    assert Error.dev_mode_only() == {:error, :dev_mode_only}
  end

  test "invalid/0 returns correct error tuple" do
    assert Error.invalid() == {:error, :invalid}
  end

  property "invalid/1 returns correct error tuple" do
    check all arg <- StreamData.term() do
      assert Error.invalid(arg) == {:error, :invalid, arg}
    end
  end

  test "invalid_currency/0 returns correct error tuple" do
    assert Error.invalid_currency() == {:error, :invalid_currency}
  end

  test "invalid_currency_amount/0 returns correct error tuple" do
    assert Error.invalid_currency_amount() == {:error, :invalid_currency_amount}
  end

  test "invalid_module/0 returns correct error tuple" do
    assert Error.invalid_module() == {:error, :invalid_module}
  end

  test "no_record/0 returns correct error tuple" do
    assert Error.no_record() == {:error, nil}
  end

  test "not_implemented/0 returns correct error tuple" do
    assert Error.not_implemented() == {:error, :not_implemented}
  end
end
