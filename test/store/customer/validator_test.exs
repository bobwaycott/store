defmodule Store.CustomerValidatorTest do
  alias Store.Customer.Meta

  @data Meta.data()
  @validator Meta.validator()

  use Store.ValidatorCase, data: @data, validator: @validator

  test "validate_create/1 rejects missing TOS" do
    refute @validator.validate_create(@data.no_tos()).valid?
  end

  test "validate_create/1 rejects missing email" do
    refute @validator.validate_create(@data.no_email()).valid?
  end

  test "validate_create/1 rejects invalid email" do
    refute @validator.validate_create(@data.invalid_email()).valid?
  end

  test "validate_create/1 rejects missing name" do
    refute @validator.validate_create(@data.no_name()).valid?
  end

  test "validate_create/1 rejects missing source" do
    refute @validator.validate_create(@data.no_source()).valid?
  end

  test "validate_update/1 rejects invalid email" do
    refute @validator.validate_update(@data.invalid_email()).valid?
  end
end
