defmodule Store.CustomerMetaTest do
  alias Store.Customer.Meta
  use Store.StoreCase

  test "has cast/0" do
    assert Meta.cast() == Store.Customer.Cast
  end

  test "has create_payload/0 for docs" do
    payload = Meta.create_payload()
    assert is_binary(payload)

    assert_value(
      payload == """
      %{
        tos: true,
        email: \"customer@example.org\",
        name: \"Happy Example Customer\",
        source: \"tok_visa\"
      }
      """
    )
  end

  test "has data/0" do
    assert Meta.data() == Store.CustomerFixture
  end

  test "data/0 is nil in live mode" do
    Mix.env(:prod)
    assert_value(Meta.data() == nil)
    Mix.env(:test)
  end

  test "has entity/0" do
    assert Meta.entity() == Stripe.Customer
  end

  test "has lookups/0" do
    assert is_list(Meta.lookups())
    assert_value(Meta.lookups() == [:stripe_id])
  end

  test "lookups include email when not in zero-knowledge mode" do
    Application.put_env(:store, :zero_knowledge, false)
    assert_value(Meta.lookups() == [:email, :stripe_id])
    Application.put_env(:store, :zero_knowledge, true)
  end

  test "has mod/0" do
    assert Meta.mod() == Store.Customer
  end

  test "has record/0" do
    assert Meta.record() == Store.Customer.Schema
  end

  test "has thing/0" do
    assert_value(Meta.thing() == "customer")
  end

  test "has things/0" do
    assert_value(Meta.things() == "customers")
  end

  test "has update_payload/0" do
    payload = Meta.update_payload()
    assert is_binary(payload)

    assert_value(
      payload == """
      %{
        email: \"customer@example.org\",
        name: \"Happiest Example Customer\",
        source: \"tok_mastercard\"
      }
      """
    )
  end

  test "has validator/0" do
    assert Meta.validator() == Store.Customer.Validator
  end
end
