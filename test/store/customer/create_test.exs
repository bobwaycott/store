defmodule CustomerCreateTest do
  alias Store.Customer.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.CreateCase, main: @main, data: @data

  test "create/0 fails when in live mode" do
    Mix.env(:prod)
    assert Store.Config.live_mode?() == true
    res = @main.create()
    assert res == Error.dev_mode_only()
    Mix.env(:test)
  end

  test "create fails when terms not agreed to" do
    with data <- @data.no_tos() do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  test "create fails when email empty" do
    with data <- @data.no_email() do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  test "create fails when email invalid" do
    with data <- @data.invalid_email() do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  @tag :no_zero
  test "create!/1 fails when email taken" do
    with data <- %{@data.valid_payload() | email: @data.known().email} do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  test "create fails when name invalid" do
    with data <- @data.no_name() do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  test "create fails when source invalid" do
    with data <- @data.no_source() do
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(data)
      assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(data)
    end
  end

  @tag :stripe_api
  test "create/0 succeeds when not in live mode" do
    {:ok, customer} = @main.create()
    refute is_nil(customer.record)
    refute is_nil(customer.entity)
  end

  # Verifies payload persisted correctly
  defp verify_record(data, customer) do
    %{record: record, entity: _} = customer
    refute is_nil(record)

    unless Store.Config.zero_knowledge?() do
      assert_value record.email == data.email
      assert_value record.description == data.description
    end

    assert_value record.tos == data.tos
  end

  # Verifies record keys match their Stripe entity counterparts.
  defp verify_entity(customer) do
    %{record: record, entity: entity} = customer
    refute is_nil(entity)

    unless Store.Config.zero_knowledge?() do
      assert_value record.email == entity.email
      assert_value record.description == entity.description
    end

    assert_value record.stripe_id == entity.id
    assert_value record.stripe_source == entity.default_source
  end
end
