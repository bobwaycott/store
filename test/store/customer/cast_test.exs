defmodule Store.CustomerCastTest do
  use Store.StoreCase
  alias Store.Customer.Meta

  @cast Meta.cast()
  @data Meta.data()
  @entity @data.entity()
  @payload @data.entity_payload()

  test "entity/1 returns Stripe entity keys only" do
    entity = @cast.entity(@payload)

    assert_value Map.keys(entity) == [
                   :account_balance,
                   :description,
                   :email,
                   :invoice_prefix,
                   :invoice_settings,
                   :metadata,
                   :shipping,
                   :source
                 ]
  end

  test "record_create/2 returns correctly remapped fields for database" do
    res = @cast.record_create(@entity, @payload)
    assert_value Map.keys(res) == [:stripe_id, :stripe_source, :tos]
    assert_value @entity.id == res.stripe_id
    assert_value @entity.default_source == res.stripe_source
  end

  test "record_create/2 returns extra fields when not in zero-knowledge mode" do
    Application.put_env(:store, :zero_knowledge, false)
    res = @cast.record_create(@entity, @payload)
    assert_value Map.keys(res) == [:description, :email, :stripe_id, :stripe_source, :tos]
    Application.put_env(:store, :zero_knowledge, true)
  end

  test "record_update/1 returns correctly remapped fields for database" do
    res = @cast.record_update(@entity)
    assert_value Map.keys(res) == [:stripe_id, :stripe_source]
    assert_value @entity.id == res.stripe_id
    assert_value @entity.default_source == res.stripe_source
  end

  test "record_update/1 returns extra fields when not in zero-knowledge mode" do
    Application.put_env(:store, :zero_knowledge, false)
    res = @cast.record_update(@entity)
    assert_value Map.keys(res) == [:description, :email, :stripe_id, :stripe_source]
    Application.put_env(:store, :zero_knowledge, true)
  end
end
