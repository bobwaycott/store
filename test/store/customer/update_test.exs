defmodule CustomerUpdateTest do
  alias Store.Customer.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.UpdateCase, main: @main, data: @data

  # Verifies record keys match their Stripe entity counterparts.
  defp verify_update(data, customer) do
    %{record: record, entity: entity} = customer
    refute is_nil(record)
    refute is_nil(entity)

    unless Store.Config.zero_knowledge?() do
      assert_value record.email == entity.email
      assert_value record.description == entity.description
    end

    assert_value record.stripe_source == entity.default_source
    assert_value data.account_balance == entity.account_balance
    assert_value data.email == entity.email
    assert_value data.description == entity.description
    assert data.metadata == entity.metadata
    assert data.shipping == entity.shipping
  end
end
