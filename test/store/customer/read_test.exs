defmodule CustomerReadTest do
  alias Store.Customer.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.ReadCase, main: @main, data: @data

  @known @data.known()

  @tag :no_zero
  test "email_available?/1 returns true when email is not present in db" do
    res =
      Faker.Internet.email()
      |> @main.email_available?()

    assert_value res == true
  end

  @tag :no_zero
  test "email_available?/1 returns false when email is present in db" do
    assert_value @main.email_available?(@known.email) ==
                   {:error, "This email has already been taken."}
  end

  @tag :no_zero
  test "get/1 succeeds when email exists" do
    record = @main.get(@known.email)
    refute is_nil(record)
  end

  @tag :no_zero
  test "get!/1 succeeds when email exists" do
    record = @main.get!(@known.email)
    refute is_nil(record)
  end
end
