defmodule CustomerDeleteTest do
  alias Store.Customer.Meta

  @main Meta.mod()
  @data Meta.data()

  use Store.DeleteCase, main: @main, data: @data

  @tag :stripe_api
  test "delete/1 succeeds when email exists" do
    unless Store.Config.zero_knowledge?() do
      {:ok, created} = @main.create()
      assert {:ok, _thing} = @main.delete(created.record.email)
    end
  end

  @tag :stripe_api
  test "delete!/1 succeeds when email exists" do
    unless Store.Config.zero_knowledge?() do
      {:ok, created} = @main.create()
      assert _thing = @main.delete!(created.record.email)
    end
  end
end
