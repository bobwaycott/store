defmodule CustomerTest do
  use Store.StoreCase
  alias Store.Customer.Meta
  alias Store.Schema.{Order, Product, Sku}

  @main Meta.mod()
  @data Meta.data()
  @known @data.known()
  @record Meta.record()

  @tag :no_zero
  test "email_available?/1 returns true when email is not present in db" do
    res =
      Faker.Internet.email()
      |> @main.email_available?()

    assert_value res == true
  end

  @tag :no_zero
  test "email_available?/1 returns false when email is present in db" do
    assert_value @main.email_available?(@known.email) ==
                   {:error, "This email has already been taken."}
  end

  test "change/1 accepts empty customer record and returns changeset" do
    res =
      @main.change(%@record{})
      |> inspect(limit: :infinity, printable_limit: :infinity)

    assert_value res == File.read!("test/results/Store.Customer.change:1-empty.txt")
  end

  test "change/1 does not accept other record types" do
    res =
      Enum.take([Order, Product, Sku], 1)
      |> @main.change()

    assert_value res == Error.invalid()
  end
end
