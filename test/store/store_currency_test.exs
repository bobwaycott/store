defmodule StoreCurrencyTest do
  use Store.StoreCase

  alias Store.Currency, as: Currency

  doctest Currency

  test "can identify supported currencies" do
    for currency <- Currency.supported() do
      assert Currency.valid?(currency)
    end
  end

  test "can identify currencies that aren't supported by AMEX" do
    for currency <- Currency.no_amex() do
      refute Currency.can_use_amex?(currency)
    end
  end

  test "returns valid default currency atom when configured and valid atom or string" do
    assert_value Currency.default() == :USD
    Application.put_env(:store, :default_currency, :USD)
    assert_value Currency.default() == :USD
    Application.put_env(:store, :default_currency, :usd)
    assert_value Currency.default() == :USD
    Application.put_env(:store, :default_currency, "usd")
    assert_value Currency.default() == :USD
    Application.put_env(:store, :default_currency, "USD")
    assert_value Currency.default() == :USD
    Application.put_env(:store, :default_currency, :USD)
  end

  test "rejects invalid default currency" do
    Application.put_env(:store, :default_currency, "foo")
    assert Currency.default() == Error.invalid_currency()
    Application.put_env(:store, :default_currency, :USD)
    assert Currency.default() == :USD
  end

  test "format/2 formats default currency when currency is missing" do
    assert_value Currency.format(123.45) == "$123.45"
  end

  test "format/2 formats specific currency correctly" do
    assert_value Currency.format(12345, :JPY) == "¥12,345"
  end

  test "format_stripe/2 formats stripe integer amount to default currency when currency is missing" do
    assert_value Currency.format_stripe(12345) == "$123.45"
  end

  test "format_stripe/2 formats stripe integer amount specific currency correctly" do
    assert_value Currency.format_stripe(12345, :JPY) == "¥12,345"
  end

  property "to_int/2 accepts integers for decimal currency" do
    check all amount <- StreamData.positive_integer() do
      refute Currency.to_int(amount) == Error.invalid_currency()
    end
  end

  property "to_int/2 accepts floats for decimal currency" do
    check all amount <- StreamData.positive_integer() do
      refute Currency.to_int(amount * 1.0) == Error.invalid_currency()
    end
  end

  property "to_int/2 accepts Decimals for decimal currency" do
    check all amount <- StreamData.positive_integer() do
      refute Currency.to_int(Decimal.new(1, amount, 0)) == Error.invalid_currency()
    end
  end

  test "returns 0 when to_int is 0" do
    assert 0 == Currency.to_int(0.0)
    assert 0 == Currency.to_int(0, :JPY)
  end
end
