# Make sure a person can't ever mistakenly use the wrong API key
with api_key <- Application.fetch_env!(:stripity_stripe, :api_key) do
  if String.contains?(api_key, "_live_") do
    raise Store.Error.DangerZone
  end
end

ExUnit.start()
