defmodule Store.ValidatorCase do
  @moduledoc """
  Defines setup and test functions for validation functions.
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @data Keyword.get(opts, :data)
      @validator Keyword.get(opts, :validator)

      test "validate_create/1 rejects empty payload" do
        refute @validator.validate_create(%{}).valid?
      end

      test "validate_create/1 rejects invalid payload" do
        refute @validator.validate_create(@data.invalid_payload()).valid?
      end

      test "validate_create/1 accepts minimal valid payload" do
        assert @validator.validate_create(@data.valid_payload()).valid?
      end

      test "validate_create/1 accepts full valid payload" do
        assert @validator.validate_create(@data.full_payload()).valid?
      end
    end
  end
end
