defmodule Store.StripeMockTest do
  use ExUnit.Case, async: true
  alias Store.StripeMock
  require StripeMock

  defp assert_port_open(port, time \\ 250) do
    Process.sleep(time)
    assert {:ok, socket} = :gen_tcp.connect('localhost', port, [])
    :gen_tcp.close(socket)
  end

  @tag :stripe_mock
  test "stripe-mock started in test helper by default" do
    sm = Process.whereis(StripeMock)
    refute sm == nil
    assert_port_open(12123)
    StripeMock.stop(sm)
  end

  @tag :stripe_mock
  test "stripe-mock can be started globally and defaults to port 12111" do
    assert {:ok, pid} = StripeMock.start_link(global: true)
    assert Process.whereis(StripeMock) == pid
    assert_port_open(12111)
    StripeMock.stop(pid)
  end

  @tag :stripe_mock
  test "stripe-mock can be started locally" do
    assert {:ok, pid} = StripeMock.start_link()
    refute Process.whereis(StripeMock) == pid
    assert_port_open(12111)
    StripeMock.stop(pid)
  end

  @tag :stripe_mock
  test "stripe-mock can start on specific port" do
    assert {:ok, pid} = StripeMock.start_link(port: 12123)
    assert_port_open(12123)
    StripeMock.stop(pid)
  end

  @tag :stripe_mock
  test "stripe-mock can reset" do
    assert {:ok, pid} = StripeMock.start_link(port: 12123)
    assert_port_open(12123)
    assert :ok = StripeMock.reset(pid)
    assert_port_open(12123)
    StripeMock.stop(pid)
  end
end
