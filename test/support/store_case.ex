defmodule Store.StoreCase do
  @moduledoc """
  Defines basic setup for standard `Store` tests.
  """
  use ExUnit.CaseTemplate

  using do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      alias Store.Error
      import AssertValue
    end
  end
end
