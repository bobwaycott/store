defmodule Store.StripeMock do
  @moduledoc """
  Create a GenServer that mocks interacting with the Stripe API.
  """
  use GenServer
  require Logger

  def start_link(opts \\ []) do
    start_opts =
      case opts[:global] do
        true ->
          [name: __MODULE__]

        _ ->
          []
      end

    GenServer.start_link(__MODULE__, opts, start_opts)
  end

  def reset(pid \\ __MODULE__) do
    GenServer.call(pid, :reset)
  end

  def stop(pid \\ __MODULE__) do
    GenServer.stop(pid)
  end

  @impl GenServer
  def handle_call(:reset, from, %{mgr_pid: mgr_pid, os_pid: os_pid} = state) do
    kill_stripe_mock(mgr_pid)
    {:noreply, %{state | mgr_pid: mgr_pid, os_pid: os_pid, restarting: {true, from}}}
  end

  @impl GenServer
  def handle_info({:stderr, os_pid, msg}, %{os_pid: os_pid} = state) do
    Logger.debug("[stripe-mock] #{String.trim(msg)}")
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:stdout, os_pid, msg}, %{os_pid: os_pid} = state) do
    Logger.debug("[stripe-mock:out] #{String.trim(msg)}")
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(
        {:DOWN, os_pid, :process, _ex_pid, _reason},
        %{os_pid: os_pid, opts: opts, restarting: {true, from}} = state
      ) do
    {:ok, mgr_pid, os_pid} = start_stripe_mock(opts)
    GenServer.reply(from, :ok)
    {:noreply, %{state | mgr_pid: mgr_pid, os_pid: os_pid, restarting: false}}
  end

  @impl GenServer
  def init(opts) do
    {:ok, mgr_pid, os_pid} = start_stripe_mock(opts)
    {:ok, %{mgr_pid: mgr_pid, os_pid: os_pid, opts: opts, restarting: false}}
  end

  @impl GenServer
  def terminate(_reason, %{mgr_pid: mgr_pid}) do
    Logger.debug("Terminate StripeMock")
    kill_stripe_mock(mgr_pid)
  end

  defp start_stripe_mock(opts) do
    exec = opts[:stripe_mock_path] || System.find_executable("stripe-mock")

    unless exec do
      raise("Could not find stripe-mock. Check your PATH or pass :stripe_mock_path option.")
    end

    port = opts[:port] || 12111

    port_args =
      case port do
        nil -> []
        port when is_number(port) -> ["-port", port]
        _ -> raise "stripe-mock port must be a number."
      end

    Logger.debug("Starting stripe-mock on port #{port}")

    [exec | port_args]
    |> Exexec.run(monitor: true, stdout: true, stderr: true)
  end

  defp kill_stripe_mock(mgr_pid) do
    Logger.debug("Killing stripe-mock")

    case Exexec.stop(mgr_pid) do
      :ok ->
        :ok

      {:error, err} ->
        Logger.error("Could not kill stripe-mock: #{inspect(err)}")
        :ok
    end
  end
end
