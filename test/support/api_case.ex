defmodule Store.APICase do
  @moduledoc """
  Defines setup for update tests. Used to test `Store.<module>` top-level interfaces.
  """

  defmacro __using__(opts) do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @moduletag unquote(opts)
    end
  end
end
