defmodule Store.UpdateCase do
  @moduledoc """
  Defines setup and test functions for `Store.Updatable` functions. Used to test `Store.Updatable` implementations.
  """
  @callback verify_update(map, struct) :: nil

  defmacro __using__(opts) do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @moduletag unquote(opts)

      property "update/1 fails when not integer or binary id", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.update(arg, %{}) == Error.invalid()
        end
      end

      property "update!/1 fails when not integer or binary id", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.update!(arg, %{}) == Error.invalid()
        end
      end

      property "update/1 fails when payload is not map", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_map(&1))) do
          refute is_map(arg)
          assert_value main.update(1, arg) == Error.invalid()
        end
      end

      property "update!/1 fails when payload is not map", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_map(&1))) do
          refute is_map(arg)
          assert_value main.update!(1, arg) == Error.invalid()
        end
      end

      @tag :stripe_api
      test "update/1 succeeds when pk exists", %{main: main, data: data} do
        {:ok, created} = main.create()
        data = data.full_payload()
        {:ok, thing} = main.update(created.record.id, data)
        verify_update(data, thing)
      end

      @tag :stripe_api
      test "update!/1 succeeds when pk exists", %{main: main, data: data} do
        {:ok, created} = main.create()
        data = data.full_payload()
        {:ok, thing} = main.update!(created.record.id, data)
        verify_update(data, thing)
      end

      @tag :stripe_api
      test "update/1 succeeds when Stripe id exists", %{
        main: main,
        data: data
      } do
        {:ok, created} = main.create()
        data = data.full_payload()
        {:ok, thing} = main.update(created.record.stripe_id, data)
        verify_update(data, thing)
      end

      @tag :stripe_api
      test "update!/1 succeeds when Stripe id exists", %{
        main: main,
        data: data
      } do
        {:ok, created} = main.create()
        data = data.full_payload()
        {:ok, thing} = main.update!(created.record.stripe_id, data)
        verify_update(data, thing)
      end
    end
  end
end
