defmodule Store.ReadCase do
  @moduledoc """
  Defines setup and test functions for `Store.Readable` functions. Used to test `Store.Readable` implementations.
  """

  defmacro __using__(opts) do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @moduletag unquote(opts)

      property "get/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.get(arg) == Error.invalid()
        end
      end

      property "get!/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.get!(arg) == Error.invalid()
        end
      end

      test "get/1 succeeds when pk exists", %{main: main, data: data} do
        data = data.known()
        {:ok, thing} = main.get(data.id)
        refute is_nil(thing.record)
      end

      test "get!/1 succeeds when pk exists", %{main: main, data: data} do
        data = data.known()
        thing = main.get!(data.id)
        refute is_nil(thing.record)
      end

      test "get/1 succeeds when Stripe id exists", %{main: main, data: data} do
        data = data.known()
        {:ok, thing} = main.get(data.stripe_id)
        refute is_nil(thing.record)
      end

      test "get!/1 succeeds when Stripe id exists", %{main: main, data: data} do
        data = data.known()
        thing = main.get!(data.stripe_id)
        refute is_nil(thing.record)
      end

      property "get_with_entity!/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.get_with_entity!(arg) == Error.invalid()
        end
      end

      property "get_with_entity/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.get_with_entity(arg) == Error.invalid()
        end
      end

      @tag :stripe_api
      test "get_with_entity!/1 succeeds when record exists", %{
        main: main,
        data: data
      } do
        payload = data.valid_payload()
        created = main.create!(payload)
        retrieved = main.get_with_entity!(created.record.id)
        assert created == retrieved
      end

      @tag :stripe_api
      test "get_with_entity/1 succeeds when record exists", %{
        main: main,
        data: data
      } do
        payload = data.valid_payload()
        created = main.create!(payload)
        {:ok, retrieved} = main.get_with_entity(created.record.id)
        assert created == retrieved
      end
    end
  end
end
