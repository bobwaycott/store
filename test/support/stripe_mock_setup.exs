# Stripe.start
Application.ensure_all_started(:erlexec)
Application.ensure_all_started(:exexec)
Application.ensure_all_started(:mox)
Logger.configure(level: :info)

{:ok, pid} = Store.StripeMock.start_link(port: 12123, global: true)
IO.inspect(pid, label: "[stripe-mock] server")

Application.put_env(:stripity_stripe, :api_base_url, "http://localhost:12123/v1/")
Application.put_env(:stripity_stripe, :api_upload_url, "http://localhost:12123/v1/")
Application.put_env(:stripity_stripe, :api_key, "sk_test_123")
Application.put_env(:stripity_stripe, :log_level, :debug)

Mox.defmock(Stripe.Connect.OAuthMock, for: Stripe.Connect.OAuth)
Mox.defmock(Stripe.APIMock, for: Stripe.API)

defmodule StripeHelper do
  @fixture_path "./test/fixtures/"

  def load_fixture(filename) do
    File.read!(@fixture_path <> filename)
    |> Stripe.API.json_library().decode!()
  end

  def wait_until_stripe_mock_launch() do
    case Stripe.Charge.list() do
      {:error, %Stripe.Error{code: :network_error}} ->
        # It might be connection refused.
        Process.sleep(250)
        wait_until_stripe_mock_launch()

      _ ->
        true
    end
  end
end

StripeHelper.wait_until_stripe_mock_launch()
