defmodule Store.StripeMockCase do
  @moduledoc """
  Defines setup for tests requiring access to a mocked version of the Stripe API.
  """
  use ExUnit.CaseTemplate

  def assert_stripe_requested(_method, _url, _extra \\ []) do
    assert true
  end

  def stripe_base_url() do
    Application.get_env(:stripity_stripe, :api_base_url)
  end

  using do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      alias Store.Error
      import AssertValue

      import Store.StripeMockCase,
        only: [assert_stripe_requested: 2, assert_stripe_requested: 3, stripe_base_url: 0]
    end
  end
end
