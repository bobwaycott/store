defmodule Store.DeleteCase do
  @moduledoc """
  Defines setup and test functions for `Store.Deletable` functions. Used to test `Store.Deletable` implementations.
  """

  defmacro __using__(opts) do
    quote do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @moduletag unquote(opts)

      property "delete/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.delete(arg) == Error.invalid()
        end
      end

      property "delete!/1 fails when not integer or binary", %{main: main} do
        check all arg <-
                    StreamData.term()
                    |> StreamData.filter(&(not is_binary(&1)))
                    |> StreamData.filter(&(not is_integer(&1))) do
          refute is_binary(arg)
          refute is_integer(arg)
          assert_value main.delete!(arg) == Error.invalid()
        end
      end

      @tag :stripe_api
      test "delete/1 succeeds when pk exists", %{main: main, data: data} do
        {:ok, created} = main.create()
        {:ok, thing} = main.delete(created.record.id)
      end

      @tag :stripe_api
      test "delete!/1 succeeds when pk exists", %{main: main, data: data} do
        {:ok, created} = main.create()
        thing = main.delete!(created.record.id)
      end

      @tag :stripe_api
      test "delete/1 succeeds when Stripe id exists", %{
        main: main,
        data: data
      } do
        {:ok, created} = main.create()
        {:ok, thing} = main.delete(created.record.stripe_id)
      end

      @tag :stripe_api
      test "delete!/1 succeeds when Stripe id exists", %{
        main: main,
        data: data
      } do
        {:ok, created} = main.create()
        thing = main.delete!(created.record.stripe_id)
      end
    end
  end
end
