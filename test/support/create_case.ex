defmodule Store.CreateCase do
  @moduledoc """
  Defines setup and test functions for `Store.Creatable` functions. Used to test `Store.Creatable` implementations.
  """
  @callback verify_record(map, struct) :: nil
  @callback verify_entity(struct) :: nil

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      use ExUnit.Case, async: true
      use ExUnitProperties

      import AssertValue

      alias Store.Error

      @data Keyword.get(opts, :data)
      @main Keyword.get(opts, :main)

      test "create fails when empty POST" do
        assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(%{})
        assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(%{})
      end

      test "create fails when POST missing keys" do
        with payload <- @data.invalid_payload() do
          assert {:error, :invalid, %Ecto.Changeset{}} = @main.create!(payload)
          assert {:error, :invalid, %Ecto.Changeset{}} = @main.create(payload)
        end
      end

      @tag :stripe_api
      test "create/1 succeeds when valid POST" do
        payload = @data.valid_payload()
        {:ok, thing} = @main.create(payload)
        verify_record(payload, thing)
        verify_entity(thing)
      end

      @tag :stripe_api
      test "create!/1 succeeds when valid POST" do
        payload = @data.valid_payload()
        thing = @main.create!(payload)
        verify_record(payload, thing)
        verify_entity(thing)
      end
    end
  end
end
