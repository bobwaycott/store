alias Store.Repo

:rand.seed(:default, :os.timestamp())

for x <- 1..10 do
  alias Store.Customer.Schema
  alias Store.CustomerFixture, as: Data
  data = if x > 1, do: Data.seed(), else: Data.known()

  Schema.changeset(%Schema{}, data)
  |> Repo.insert!()
end

for x <- 1..10 do
  alias Store.Product.Schema
  alias Store.ProductFixture, as: Data
  data = if x > 1, do: Data.seed(), else: Data.known()

  Schema.changeset(%Schema{}, data)
  |> Repo.insert!()
end
