defmodule Store.Repo.Migrations.CreateStoreTables do
  use Ecto.Migration

  @zero Store.Config.zero_knowledge?()

  def change do
    create table(:store_customers) do
      add(:tos, :boolean, null: false, default: false)

      unless @zero do
        add(:email, :text, null: false)
        add(:description, :text, null: false)
      end

      add(:stripe_id, :string, null: false)
      add(:stripe_source, :string, null: false)

      timestamps()
    end

    unless @zero do
      create(index(:store_customers, [:email], unique: true))
    end

    create(index(:store_customers, [:stripe_id], unique: true))

    create table(:store_products) do
      add(:active, :boolean, null: false, default: true)
      add(:attributes, {:array, :text})
      add(:caption, :text)
      add(:description, :text)
      add(:images, {:array, :text})
      add(:name, :text, null: false)
      add(:stripe_id, :string, null: false)
      timestamps()
    end

    create(index(:store_products, [:stripe_id], unique: true))

    create table(:store_skus) do
      add(:active, :boolean, null: false, default: true)
      add(:attributes, :map)
      add(:currency, :string)
      add(:image, :string)
      add(:inventory, :map, null: false)
      add(:price, :integer, null: false)
      add(:stripe_id, :string, null: false)
      add(:product_id, references(:store_products))
      timestamps()
    end

    create(index(:store_skus, [:stripe_id], unique: true))

    create table(:store_orders) do
      add(:stripe_id, :string, null: false)
      add(:customer_id, references(:store_customers))
      timestamps()
    end

    create(index(:store_orders, [:stripe_id], unique: true))
  end
end
