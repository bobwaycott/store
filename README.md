# Store

`Store` is a pluggable Elixir application that interfaces with the Stripe API

It relies on the `:stripity_stripe` Stripe library for Elixir, providing a (hopefully) easy way to integrate Stripe into an application that wants to handle charging customers for goods/services.

## Why the separate app?

This is a bit of an experiment in decoupling apps from a Phoenix application that provides a web frontend for ordering.

The ultimate goal is to experiment with making Stripe integration a relatively drop-in action for any Elixir/Phoenix web app by turning all the complex Stripe + local databse work into something you include as a library.

## Try things out

Clone the repo to your local machine:

```shell
# clone repo
$ git clone git@gitlab.com:bobwaycott/store.git

# change directory
$ cd store

# fetch Mix deps
$ mix deps.get

# compile
$ mix

# setup database; you may need to update postgres settings in dev/test config
$ mix ecto.create
$ mix ecto.migrate

# update config/dev.secret.exs — replace sk_test_SECRET with valid Stripe key
# update config/test.secret.exs — replace sk_test_SECRET with valid Stripe key

# run iex
$ iex -S mix
```

In the `iex` session, you can create a `Customer` like so:

```elixir
Store.create(:customer, %{
  email: "foo@example.org",
  description: "example customer",
  source: "tok_visa",
  tos: true
  })
```

The result will be a `Store.Customer` struct that includes two important attributes—an `entity`, which will be a hydrated customer record *from Stripe*, and a `record` which is the *database record* from your local PostgreSQL instance.


## Documentation

This is probably the best way to get a feel for the library outside of reading code.

Documentation can be generated with:

```shell
$ mix docs && open doc/index.html
```

This will generate docs & open your browser so you can see what the library currently supports.

## Tests

To run default tests (that **will never** hit the Stripe API):

```shell
$ mix test
```

To run Stripe API tests (that **will always** hit the Stripe API):

```shell
$ mix test --only stripe_api
```

To see coverage:

```shell
$ mix coveralls
```

To see sexy HTML coverage:

```shell
$ mix coveralls.html && open cover/excoveralls.html
```
