defmodule Store do
  @moduledoc """
  Main application interface.

  Provides a consistent way of interacting with the store
  via a simple top-level API that communicates simultaneously
  with the database and Stripe.

  All `Store` functions require a `module` argument, identifying
  which `Store` module you wish to interact with.

  Valid `module` atoms are:

    - `:address`  >>  calls `Store.Address` module (*coming soon*)
    - `:customer`  >>  calls `Store.Customer` module
    - `:order`  >>  calls `Store.Order` module (*coming soon*)
    - `:product`  >>  calls `Store.Product` module (*coming soon*)
    - `:sku`  >>  calls `Store.Sku` module (*coming soon*)
    - `:source`  >>  calls `Store.Source` module (*coming soon*)
    - `:subscription`  >>  calls `Store.Subscription` module (*coming soon*)

  ## Terminology

    `Store` sticks to naming modules after their relevant Stripe counterparts.
    This is done to avoid making anyone learn unnecessary terms to map a `Store`
    thing to a `Stripe` thing. That said, to keep things somewhat sane when
    reading docs and working with `Store`, there are a couple terms worth noting:

    - A `record` always refers to something stored in (or fetched from) the `Store` database
    - An `entity` always refers to something stored in (or fetched from) Stripe
  """
  alias Store.Error

  @modules [:customer, :order, :product, :sku]

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking `Store.Schema.<MODULE>` changes.

  `Store.change/1` is intended for use in forms & other processes where you
  expect to start with an empty record. If you're looking to modify an existing
  record, use `Store.change/2`.

  ## Examples

      Store.change(:customer)
      %Ecto.Changeset{}

  """
  def change(module) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.change(%Store.Customer.Schema{})
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def change(_), do: Error.invalid_module()

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking `Store.Schema.<MODULE>` changes.

  `Store.change/2` is intended for use in forms & other processes where you
  have a non-empty record. If you're looking to work with a new record for
  forms (e.g., a `new` controller in Phoenix), use `Store.change/1`.

  ## Examples

      c = Store.get(:customer, 1234)
      Store.change(:customer, c)
      %Ecto.Changeset{}

  """
  def change(module, record) when is_atom(module) and module in @modules and is_map(record) do
    case module do
      :customer -> Store.Customer.change(record)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def change(_, _), do: Error.invalid_module()

  @doc """
  Create a record in the database and Stripe of type `module`.

  Creating records returns the database record, with an embedded
  `:entity` that is the entire Stripe entity returned via API.

  ## Examples

      Store.create(:customer, map)
      {:ok, %Store.Customer{}}


      Store.create(:asdf, map)
      Error.invalid_module()

  """
  def create(module, map) when is_atom(module) and module in @modules and is_map(map) do
    case module do
      :customer -> Store.Customer.create(map)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def create(_, _), do: Error.invalid_module()

  @doc """
  Like `create/2`, but raises on database error.
  """
  def create!(module, map) when is_atom(module) and module in @modules and is_map(map) do
    case module do
      :customer -> Store.Customer.create!(map)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def create!(_, _), do: Error.invalid_module()

  @doc """
  Deletes a customer from the database and Stripe.

  Most mdules provide support for calling `delete` with the following attributes:

    - Store `id`
    - Stripe `id`

  Some modules support other unique identifiers&mdash;for example, you can delete
  customers with email addresses. The rule is simple&mdash;if you can `get` or `get!`
  a record/entity by a particular identifier, you can `delete` with the same identifier.

  Returns `{:ok, record, entity}` tuple when successful so you can
  process further, if needed&mdash;where `record` is the database record &
  `entity` is the Stripe entity.

  Returns `Ecto` or `Stripe` errors, if any occur.

  ## Examples

      # Delete by database id
      Store.delete(:customer, 1234)
      {:ok, record, entity}

      # Delete by email address
      Store.delete(:customer, "asdf@example.org")
      {:ok, record, entity}

      # Delete by Stripe id
      Store.delete(:customer, "cus_123456")
      {:ok, record, entity}
  """
  def delete(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.delete(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def delete(_, _), do: Error.invalid_module()

  @doc """
  Same as `delete/2`, but raises on database error.
  """
  def delete!(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.delete!(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def delete!(_, _), do: Error.invalid_module()

  @doc """
  Returns a record from the database of type `module` via a single supported identifier.

  Most mdules provide support for calling `get` with the following attributes:

    - Store `id`
    - Stripe `id`

  Some modules support other unique identifiers&mdash;for example, you can get
  customers with email addresses. Check the relevant top-level `Store.<MODULE>`
  docs for more details.

  Returns `nil` if the customer does not exist in the database.

  ## Examples

      # Get by database id
      Store.get(123)
      %Store.Customer.Schema{}

      Store.get(456)
      nil

      # Get by Stripe id
      Store.get("cus_283746")
      %Store.Customer.Schema{}

      # Invalid module atom
      Store.get(:asdf, map)
      Error.invalid_module()

  """
  def get(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.get(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def get(_, _), do: Error.invalid_module()

  @doc """
  Returns a record from the database of type `module` via a single supported identifier.

  Most mdules provide support for calling `get!` with the following attributes:

    - Store `id`
    - Stripe `id`

  Some modules support other unique identifiers&mdash;for example, you can get
  customers with email addresses. Check the relevant top-level `Store.<MODULE>`
  docs for more details.

  Returns `Ecto.NoResultsError` if the customer does not exist in the database.

  ## Examples

      # Get by database id
      Store.get!(:customer, 123)
      %Store.Customer.Schema{}

      Store.get!(:customer, 456)
      ** Ecto.NoResultsError

      # Get by Stripe id
      Store.get!(:customer, "cus_283746")
      %Store.Customer.Schema{}

      # Invalid module atom
      Store.get!(:asdf, map)
      Error.invalid_module()

  """
  def get!(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.get!(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def get!(_, _), do: Error.invalid_module()

  @doc """
  Returns a database record of type `module` with embedded Stripe entity.

  Records returned by `get_with_entity/2` are the database record,
  with an embedded `:entity` returned via Stripe's API.

  Any identifier valid for use with `get/2` is valid for use with `get_with_entity/2`.

  Returns `nil` if the customer does not exist in the database.

  ## Examples

      # Get by database id
      Store.get_with_entity(123)
      %Store.Customer.Schema{..., entity: %Stripe.Customer{}}

      Store.get_with_entity(456)
      nil

      # Get by Stripe id
      Store.get_with_entity("cus_283746")
      %Store.Customer.Schema{..., entity: %Stripe.Customer{}}

      # Invalid module atom
      Store.get_with_entity(:asdf, map)
      Error.invalid_module()

  """
  def get_with_entity(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.get_with_entity(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def get_with_entity(_, _), do: Error.invalid_module()

  @doc """
  Returns a database record of type `module` with embedded Stripe entity.

  Records returned by `get_with_entity!/2` are the database record,
  with an embedded `:entity` returned via Stripe's API.

  Any identifier valid for use with `get!/2` is valid for use with `get_with_entity!/2`.

  Returns `Ecto.NoResultsError` if the customer does not exist in the database.

  ## Examples

      # Get by database id
      Store.get_with_entity!(:customer, 123)
      %Store.Customer.Schema{..., entity: %Stripe.Customer{}}

      Store.get_with_entity!(:customer, 456)
      ** Ecto.NoResultsError

      # Get by Stripe id
      Store.get_with_entity!(:customer, "cus_283746")
      %Store.Customer.Schema{..., entity: %Stripe.Customer{}}

      # Invalid module atom
      Store.get_with_entity!(:asdf, map)
      Error.invalid_module()

  """
  def get_with_entity!(module, id) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.get_with_entity!(id)
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def get_with_entity!(_, _), do: Error.invalid_module()

  @doc """
  Returns the list of records from the database of type `module`.

  `list` methods **will not** return embedded Stripe entities. Doing so
  would be increasingly expensive as stores grow, and we think (at this time)
  embedded entities ought to only be used when interacting with a single record.

  ## Examples

      Store.list(:customer)
      [%Store.Customer.Schema{}, ...]

      Store.list(:asdf)
      Error.invalid_module()

  """
  def list(module) when is_atom(module) and module in @modules do
    case module do
      :customer -> Store.Customer.list()
      _ -> Error.not_implemented()
    end
  end

  # Fail
  def list(_), do: Error.invalid_module()

  @doc """
  Updates a record of type `module` in the database and Stripe.

  ## Examples

      Store.update(:customer, customer, %{field: new_value})
      {:ok, %Store.Customer.Schema{}}

      Store.update(:customer, customer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update(module, record, attrs)
      when is_atom(module) and module in @modules and is_map(record) and is_map(attrs) do
    case module do
      :customer -> Store.Customer.update(record, attrs)
      _ -> Error.not_implemented()
    end
  end

  @doc """
  Same as `update/3`, but raises on database error.
  """
  def update!(module, record, attrs)
      when is_atom(module) and module in @modules and is_map(record) and is_map(attrs) do
    case module do
      :customer -> Store.Customer.update!(record, attrs)
      _ -> Error.not_implemented()
    end
  end
end
