defmodule Store.Token do
  @moduledoc """
  Helper library for Stripe `Source` records representing a payment source.
  """
  @good_tokens %{
    amex: "tok_amex",
    discover: "tok_discover",
    diners: "tok_diners",
    mastercard: "tok_mastercard",
    mastercard_debit: "tok_mastercard_debit",
    mastercard_prepaid: "tok_mastercard_prepaid",
    jcb: "tok_jcb",
    unionpay: "tok_unionpay",
    visa: "tok_visa",
    visa_debit: "tok_visa_debit"
  }

  @doc """
  Returns a random payment source token for dev/testing needs.

  ## Examples

      iex> random()
      "tok_amex"
  """
  def random() do
    Map.get(
      @good_tokens,
      @good_tokens
      |> Map.keys()
      |> Enum.random()
    )
  end
end
