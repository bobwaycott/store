defmodule Store.Customer.Meta do
  use Store.Meta,
    data: Store.CustomerFixture,
    entity: Stripe.Customer,
    lookups: [:email, :stripe_id]
end
