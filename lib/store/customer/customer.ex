defmodule Store.Customer do
  @meta Store.Customer.Meta

  use Store.Interface
  use Store.Creatable
  use Store.Readable
  use Store.Updatable
  use Store.Deletable

  @doc """
  Check if `email` is available for creating a new customer.
  """
  @spec email_available?(String.t()) :: boolean() | Error.t()
  def email_available?(email) when is_binary(email) do
    val =
      from(c in @meta.record,
        where: c.email == ^email,
        select: c.id
      )
      |> Repo.exists?()

    case val do
      true -> Error.email_taken()
      false -> true
    end
  end

  def email_available?(_), do: Error.email_invalid()
end
