defmodule Store.Customer.Schema do
  @moduledoc """
  Schema definition for a `Store.Customer` record.

  ## Zero-knowledge by default

  `Store` defaults to what we call **Zero-Knowledge Mode**. This means `Store` **will not** persist PII
  to the database, and omits such fields entirely from the schema. The data will still be transmitted
  and stored in your Stripe account.

  You can change this by modifying the application config like so:

  ```
  config :store, zero_knowledge: false
  ```

  ### We encourage you to protect customer privacy

  Even with Zero-Knowledge Mode disabled, `Store` will only persist a customer description and email.

  Please note, `Store` completely supports the entire Stripe API. You can safely ask for
  identifying information&mdash;but **please** do so over HTTPS/SSL&mdash;and it **will** be
  stored in Stripe. That data will naturally come back when you retrieve records via
  `Store.Customer.get_with_entity/1`. The advantage we see in this is that you can make the information
  available *when you need it* during a request lifetime, or during some other system/process action,
  while reducing the risks that your customers' information will be included in the Next Big Data Leak.

  In our view, Stripe's PCI-compliant security practices&mdash;not to mention the size of their
  security team&mdash;is likely much better than yours.

  ### What if I need to have user accounts for my store/SAAS product?

  We encourage you to really think about how to implement such needs without risking your customers'
  and users' private information. Some ideas:

  - Keep user accounts & info separate from customer records
  - Use usernames instead of email addresses
  - Hash user email addresses just like you do passwords
  - Hash email + password combos for authentication
  - Forgot passwords can match a hashed email & send a link to the email provided during the request lifecycle
  - Realize that your customers probably don't want all those emails you're sending them

  ## Schema

  `Store.Customer.Schema` provides the fields listed below.

  ### Zero-Knowledge Mode **ON** (default)

  - `:id`&mdash;primary key of the database record
  - `:tos`&mdash;provides a record that customer agreed to TOS at creation time
  - `:stripe_id`&mdash;the unique Stripe identifier of the customer
  - `:stripe_source`&mdash;the Stripe `:default_source` for payment(s)

  ### Zero-Knowledge Mode is **OFF**

  When Zero-Knowledge Mode is off, the following fields are added:

  - `:email`&mdash;the customer's email address
  - `:description`&mdash;a description to help humans know who the customer is
    - typically something like a name, but can be anything you like

  ## Relationships

  A `Store.Customer` can be associated with `Store.Order`s and `Store.Subscription`s.
  """
  use Ecto.Schema
  alias Ecto.Changeset
  alias Store.Order.Schema, as: Order

  if Store.Config.zero_knowledge?() do
    @required [:tos, :stripe_id, :stripe_source]
  else
    @required [:tos, :email, :description, :stripe_id, :stripe_source]
  end

  schema "store_customers" do
    field(:tos, :boolean, default: false)

    unless Store.Config.zero_knowledge?() do
      field(:email, :string)
      field(:description, :string)
    end

    field(:stripe_id, :string)
    field(:stripe_source, :string)

    has_many(:orders, Order, foreign_key: :customer_id)

    timestamps()
  end

  @doc """
  Returns a `Store.Customer.Schema` changeset.

  Will return an empty changeset without arguments.
  """
  def changeset(customer \\ %__MODULE__{}, attrs \\ %{}) do
    cg =
      customer
      |> Changeset.cast(attrs, @required)
      |> Changeset.validate_required(@required)
      |> Changeset.unique_constraint(:stripe_id)

    if Store.Config.zero_knowledge?() do
      cg
    else
      cg
      |> Changeset.unique_constraint(:email)
    end
  end
end
