defmodule Store.Customer.Cast do
  @moduledoc """
  Casts inputs into proper outputs for Stripe API & DB use.
  """
  alias Store.Util
  alias Store.Customer.Meta

  # Remap Stripe entity keys to Store record keys
  @record_keymap %{
    id: :stripe_id,
    default_source: :stripe_source
  }
  @entity Meta.entity()
  # Entity keys to take from a data map & pass on to Stripe API calls
  @entity_keys [
    :account_balance,
    :coupon,
    :default_source,
    :description,
    :email,
    :invoice_prefix,
    :invoice_settings,
    :metadata,
    :shipping,
    :source,
    :tax_info
  ]

  @doc """
  Returns `Map` for Stripe API insertion.
  """
  @spec entity(map) :: map
  def entity(%{} = data) do
    data
    |> Map.take(@entity_keys)
  end

  @doc """
  Returns `Map` for database insertion.
  """
  @spec record_create(@entity.t(), map) :: map
  def record_create(%@entity{} = entity, %{} = data) do
    entity
    |> Map.take(record_keys())
    |> Util.remap(@record_keymap)
    |> Map.merge(%{tos: data.tos})
  end

  @doc """
  Returns `Map` for database updates.
  """
  @spec record_update(@entity.t()) :: map
  def record_update(%@entity{} = entity) do
    entity
    |> Map.take(record_keys())
    |> Util.remap(@record_keymap)
  end

  # Return correct record keys for casting
  defp record_keys do
    if Store.Config.zero_knowledge?() do
      [:id, :default_source]
    else
      [:id, :default_source, :description, :email]
    end
  end
end
