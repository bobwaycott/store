defmodule Store.Customer.Validator do
  @moduledoc """
  Validator for `Store.Customer` structs.

  Used to validate data sent to create & update interfaces.
  """
  alias Ecto.Changeset
  alias Store.Util
  alias Store.Customer

  @entity %{
    account_balance: :integer,
    coupon: :string,
    default_source: :string,
    description: :string,
    email: :string,
    invoice_prefix: :string,
    invoice_settings: :map,
    metadata: :map,
    shipping: :map,
    source: :string,
    tax_info: :map,
    tos: :boolean
  }

  @required [
    :email,
    :description,
    :source,
    :tos
  ]

  @email_format ~r/.+@.+\..+/i

  @doc """
  Validate data sent to create functions.

  ## Validations

  - required fields are `#{inspect(@required)}`
  - `:tos` accepted
  - `::email` matches a simple formatted regex: `#{inspect(@email_format)}`
  -  if `Store.Config.zero_knowledge?` is `false`, checks `:email` has not been taken
  """
  def validate_create(payload) do
    payload
    |> to_changeset()
    |> Changeset.validate_acceptance(:tos)
    |> Changeset.validate_format(:email, @email_format)
    |> Changeset.validate_required(@required)
    |> validate_available()
  end

  @doc """
  Validate data sent to update functions.

  ## Validations

  - `::email` matches a simple formatted regex: `#{inspect(@email_format)}`
  """
  def validate_update(payload) do
    payload
    |> to_changeset()
    |> Changeset.validate_format(:email, @email_format)
  end

  # Cast data to `Ecto.Changeset`.
  defp to_changeset(payload) do
    payload = Util.clean_strings(payload)

    {%{}, @entity}
    |> Changeset.cast(payload, Map.keys(@entity))
  end

  # Optionally validates if email is available if store isn't in zero-knowledge mode.
  defp validate_available(changeset) do
    if Store.Config.zero_knowledge?() do
      changeset
    else
      case Customer.email_available?(changeset.changes[:email]) do
        {:error, msg} -> Changeset.add_error(changeset, :email, msg)
        _ -> changeset
      end
    end
  end
end
