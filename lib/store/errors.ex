defmodule Store.Error do
  @moduledoc """
  Shared errors used by Store modules.
  """
  @type t :: {:error, reason :: term}

  defmodule DangerZone do
    @moduledoc """
    Indicates you've tried to do something dangerous, like use a live Stripe key in dev/test.
    """
    defexception message: "DANGER ZONE: You just tried to use the wrong API key."
  end

  @doc """
  Indicates a call was made outside dev/test that requires being in dev/test mode.
  """
  def dev_mode_only, do: {:error, :dev_mode_only}

  @doc """
  Indicates an invalid email was provided.
  """
  def email_invalid, do: {:error, "This email is not valid."}

  @doc """
  Indicates a customer has already been created with this email.
  """
  def email_taken, do: {:error, "This email has already been taken."}

  @doc """
  Indicates a call was made with invalid arguments.
  """
  def invalid, do: {:error, :invalid}

  @doc """
  Indicate an invalid argument was found.
  """
  def invalid(arg), do: {:error, :invalid, arg}

  @doc """
  Indicates the store has an invalid currency config.
  """
  def invalid_currency(), do: {:error, :invalid_currency}

  @doc """
  Indicates an invalid amount was passed for currency type.
  """
  def invalid_currency_amount(), do: {:error, :invalid_currency_amount}

  @doc """
  Indicates a call was attempted on a module that does not exist.
  """
  def invalid_module, do: {:error, :invalid_module}

  @doc """
  Indicates a record asked for is not found.
  """
  def no_record, do: {:error, nil}

  @doc """
  Indicates a call was attempted on a module that should exist, but hasn't been implemented yet.
  """
  def not_implemented, do: {:error, :not_implemented}
end
