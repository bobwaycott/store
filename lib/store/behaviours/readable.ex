defmodule Store.Readable do
  @moduledoc false

  @doc false
  defmacro __using__(_opts) do
    quote location: :keep do
      import Ecto.Query, warn: false
      alias Store.{Error, Repo, Util}

      # Module attributes
      @entity @meta.entity
      @lookups @meta.lookups
      @mod @meta.mod
      @record @meta.record

      # Doc attributes
      @entity_string Util.docstring(@entity)
      @mod_string Util.docstring(@mod)
      @record_string Util.docstring(@record)

      # typespecs
      @type read_args :: integer | String.t()
      @type read_list :: {:ok, [@mod.t()]} | Error.t()
      @type read_result :: {:ok, @mod.t()} | nil | Error.no_record() | Error.invalid()

      @doc """
      Retrieve #{@meta.thing} from database by primary key or optional identifier(s).

      Returns a `#{@mod_string}` struct with embedded `#{@record_string}` struct.
      **Will not include the `#{@entity_string}` entity. Embeds the record only.**

      ### Optional identifier(s): `#{inspect(@lookups)}`

      ## Examples

          iex> #{@mod_string}.get(1)
          %#{@mod_string}{record: %#{@record_string}{}, entity: nil}
      """
      @spec get(read_args) :: read_result
      def get(id) when is_integer(id), do: Repo.get(@record, id) |> @mod.cast()
      def get(id) when is_binary(id), do: get_by(id, false)
      def get(_), do: Error.invalid()

      @doc """
      Like `get/1`, but raises database errors, if any.
      """
      @spec get!(read_args) :: read_result
      def get!(id) when is_integer(id), do: Repo.get!(@record, id) |> @mod.cast!()
      def get!(id) when is_binary(id), do: get_by(id, true)
      def get!(_), do: Error.invalid()

      @doc """
      Retrieve #{@meta.thing} from database & Stripe by primary key or optional identifier(s).

      Returns a `#{@mod_string}` struct with the `#{@record_string}` record and `#{@entity_string}` entity.

      ### Optional identifier(s): `#{inspect(@lookups)}`

      ## Examples

          iex> #{@mod_string}.get_with_entity(1)
          %#{@mod_string}{record: %#{@record_string}{}, entity: %#{@entity_string}{}}
      """
      @spec get_with_entity(read_args) :: read_result
      def get_with_entity(id) when is_binary(id) or is_integer(id), do: get(id) |> get_entity()
      def get_with_entity(_), do: Error.invalid()

      @doc """
      Like `get_with_entity/1`, but raises database errors, if any.
      """
      @spec get_with_entity!(read_args) :: read_result
      def get_with_entity!(id) when is_binary(id) or is_integer(id), do: get!(id) |> get_entity!()
      def get_with_entity!(_), do: Error.invalid()

      @doc """
      Retrieve a list of all #{@meta.things} from the database **only**.

      Returns a `#{@mod_string}` struct with embedded `record` structs.
      **Will not** return the associated `#{@entity_string}` entity structs.
      """
      @spec list() :: read_list
      def list() do
        with {:ok, records} <- Repo.all(@record) do
          {:ok,
           records
           |> Enum.map(fn x -> @mod.cast(x) end)}
        end
      end

      @spec get_by(String.t(), boolean) :: read_result
      defp get_by(id, bang) when is_binary(id) and is_boolean(bang) do
        id = String.trim(id)
        filters = Enum.into(@lookups, [], fn x -> {x, id} end)

        where =
          Enum.reduce(filters, @record, fn {key, val}, query ->
            from(q in query, or_where: field(q, ^key) == ^val)
          end)

        if bang do
          where
          |> Repo.one!()
          |> @mod.cast!()
        else
          where
          |> Repo.one()
          |> @mod.cast()
        end
      end

      defp get_by(_, _), do: Error.no_record()

      defp get_entity({:ok, thing}) do
        with {:ok, entity} <- @entity.retrieve(thing.record.stripe_id) do
          {:ok, %{thing | entity: entity}}
        end
      end

      defp get_entity(_), do: Error.no_record()

      defp get_entity!(thing) do
        with {:ok, thing} <- get_entity({:ok, thing}) do
          thing
        end
      end

      defoverridable get: 1, get!: 1, get_with_entity: 1, get_with_entity!: 1
    end
  end
end
