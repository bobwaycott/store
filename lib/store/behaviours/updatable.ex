defmodule Store.Updatable do
  @moduledoc false

  @doc false
  defmacro __using__(_opts) do
    quote location: :keep do
      alias Store.{Error, Repo, Util}
      # Module attributes
      @cast @meta.cast
      @entity @meta.entity
      @mod @meta.mod
      @record @meta.record
      @validator @meta.validator

      # Doc attributes
      @cast_string Util.docstring(@cast)
      @entity_string Util.docstring(@entity)
      @mod_string Util.docstring(@mod)
      @record_string Util.docstring(@record)

      # typespecs
      @type update_args :: integer | String.t()
      @type update_result :: {:ok, @mod.t()} | @mod.t() | Error.t()

      @doc """
      Update #{@meta.thing} in the database and Stripe by primary key or optional identifier(s).

      Returns a `#{@mod_string}` struct with embedded `record` and `entity` structs.

      ### Optional identifier(s): `#{inspect(@meta.lookups)}`

      ## Examples

      ```
      iex> payload = #{@meta.update_payload}
      iex> #{@mod_string}.update(1, payload)
      %#{@mod_string}{record: %#{@record_string}{}, entity: %#{@entity_string}{}}
      ```
      """
      @spec update(update_args, map) :: update_result
      def update(id, payload) when (is_integer(id) or is_binary(id)) and is_map(payload) do
        with {:ok, thing} <- @mod.get(id) do
          payload
          |> @validator.validate_update()
          |> store(thing.record, false)
        else
          err -> err
        end
      end

      def update(_, _), do: Error.invalid()

      @doc """
      Like `update/1`, but raises database errors, if any.
      """
      @spec update!(update_args, map) :: update_result
      def update!(id, payload) when (is_integer(id) or is_binary(id)) and is_map(payload) do
        thing = @mod.get!(id)

        payload
        |> @validator.validate_update()
        |> store(thing.record, true)
      end

      def update!(_, _), do: Error.invalid()

      # Store updated `record` in database.
      @spec send_to_db(map, @record.t(), @entity.t(), boolean) :: update_result
      defp send_to_db(data, record, entity, bang) do
        changeset = @record.changeset(record, data)

        if bang do
          Repo.update!(changeset)
          |> @mod.cast(entity)
        else
          with {:ok, record} <- Repo.update(changeset) do
            @mod.cast(record, entity)
          else
            err -> err
          end
        end
      end

      # Handle updating a `record` & `entity`.
      @spec store(Ecto.Changeset.t(), @record.t(), boolean) :: update_result
      defp store(%Ecto.Changeset{} = payload, record, bang) when is_boolean(bang) do
        if payload.valid? do
          stripe_data = @cast.entity(payload.changes)

          with {:ok, entity} <- @entity.update(record.stripe_id, stripe_data) do
            entity
            |> @cast.record_update()
            |> send_to_db(record, entity, bang)
          else
            err -> err
          end
        else
          Error.invalid(payload)
        end
      end

      defoverridable update: 2, update!: 2
    end
  end
end
