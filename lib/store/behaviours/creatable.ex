defmodule Store.Creatable do
  @moduledoc false

  @doc false
  defmacro __using__(_opts) do
    quote location: :keep do
      alias Store.{Error, Repo, Util}
      # Module attributes
      @cast @meta.cast
      @entity @meta.entity
      @mod @meta.mod
      @record @meta.record
      @validator @meta.validator

      # Doc attributes
      @cast_string Util.docstring(@cast)
      @entity_string Util.docstring(@entity)
      @mod_string Util.docstring(@mod)
      @record_string Util.docstring(@record)

      # typespecs
      @type create_result :: {:ok, @mod.t()} | @mod.t() | Error.t()

      @doc """
      Simple way to create a #{@meta.thing} in `:dev/:test` modes **only**.

      Returns `{:error, :dev_mode_only}` if used in production.
      """
      @spec create() :: create_result
      def create do
        unless Store.Config.live_mode?() do
          @meta.data().valid_payload()
          |> create()
        else
          Error.dev_mode_only()
        end
      end

      @doc """
      Create #{@meta.thing} in the database and Stripe.

      Returns a `#{@mod_string}` struct with embedded `record` and `entity` structs.

      ## Examples

      ```
      iex> payload = #{@meta.create_payload}
      iex> #{@mod_string}.create(payload)
      %#{@mod_string}{record: %#{@record_string}{}, entity: %#{@entity_string}{}}
      ```
      """
      @spec create(map) :: create_result
      def create(payload) when is_map(payload) do
        payload
        |> @validator.validate_create()
        |> store(false)
      end

      def create(_), do: Error.invalid()

      @doc """
      Like `create/1`, but raises database errors, if any.
      """
      @spec create!(map) :: create_result
      def create!(payload) when is_map(payload) do
        payload
        |> @validator.validate_create()
        |> store(true)
      end

      def create!(_), do: Error.invalid()

      # Store customer in database
      @spec send_to_db(map, @entity.t(), boolean) :: create_result
      defp send_to_db(data, entity, bang) do
        changeset = @record.changeset(%@record{}, data)

        if bang do
          Repo.insert!(changeset)
          |> @mod.cast!(entity)
        else
          with {:ok, record} <- Repo.insert(changeset) do
            @mod.cast(record, entity)
          else
            err -> err
          end
        end
      end

      # Handle storing a `record` & `entity`.
      @spec store(Ecto.Changeset.t(), boolean) :: create_result
      defp store(%Ecto.Changeset{} = payload, bang) when is_boolean(bang) do
        if payload.valid? do
          stripe_data = @cast.entity(payload.changes)

          with {:ok, entity} <- @entity.create(stripe_data) do
            entity
            |> @cast.record_create(payload.changes)
            |> send_to_db(entity, bang)
          else
            err -> err
          end
        else
          Error.invalid(payload)
        end
      end

      defoverridable create: 1, create!: 1
    end
  end
end
