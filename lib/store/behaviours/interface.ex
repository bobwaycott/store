defmodule Store.Interface do
  @moduledoc false

  @doc false
  defmacro __using__(_opts) do
    quote location: :keep do
      # alias all CRUD submodules into general API
      alias Store.{Error, Util}

      use Store.Record, record: @meta.record, entity: @meta.entity

      # Doc attributes
      @entity_string Util.docstring(@meta.entity)
      @mod_string Util.docstring(@meta.mod)
      @record_string Util.docstring(@meta.record)

      if is_nil(Module.get_attribute(__MODULE__, :moduledoc)) do
        @moduledoc """
        Top-level public API for interacting with #{@meta.things}.

        Handles lookups and persistence to Stripe & the database. **No need for separate calls!**

        It is **strongly advised** to only use top-level `Store` API functions, as underlying
        implementations may change. The top-level API is meant to remain stable. If you aren't sure where to begin, start with the `Store` documentation.

        If that doesn't work, it's likely a bug. You should only need to use
        the `#{@mod_string}` API **if and only if** you're using a very specific
        public function that isn't exposed at the top level because it is
        unique to this module.

        ## Important terms:

          `record`: A `#{@record_string}` record from the database

          `entity`: A `#{@entity_string}` entity from the Stripe API
        """
      end

      @doc """
      Returns an `Ecto.Changeset` for tracking `#{@record_string}` changes.

      Will return an error if you pass an incorrect record type.

      ## Examples

      ```
      iex> #{@mod_string}.change()
      %Ecto.Changeset{}

      iex> #{@mod_string}.change(%#{@record_string}{})
      %Ecto.Changeset{}

      iex> #{@mod_string}.change(%SomeOtherThing{})
      {:error, :invalid}
      ```
      """
      def change(thing \\ %@record{})

      def change(%@record{} = thing) do
        @record.changeset(thing, %{})
      end

      def change(_), do: Error.invalid()
    end
  end
end
