defmodule Store.Meta do
  @moduledoc false

  @doc false
  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts] do
      alias Ecto.Changeset
      alias Store.{Error, Util}

      @dialyzer {:nowarn_function, lookups: 0}

      [base, thing, _] = Module.split(__MODULE__)
      @base base
      @thing thing
      @mod Module.concat(@base, @thing)

      @data Keyword.get(opts, :data)
      @entity Keyword.get(opts, :entity)
      @lookups Keyword.get(opts, :lookups)
      @mod_string Util.docstring(@mod)

      if is_nil(Module.get_attribute(__MODULE__, :moduledoc)) do
        @moduledoc """
        Defines shared meta properties for `#{@mod_string}`.

        Meta functions return modules or values that are passed into behaviors.
        These modules/values are used for generating module functionality and docs.
        """
      end

      @doc """
      Returns `#{@mod_string}.Cast`.
      """
      def cast, do: Module.concat(@mod, "Cast")

      @doc """
      Returns a sample map that creates a `#{@mod_string}`.
      """
      def create_payload do
        @data.create_payload()
      end

      @doc """
      Returns the `#{@mod_string}Fixture` module for data generation.

      Returns `nil` when `Store.Config.live_mode?` is `true`.
      """
      def data do
        if Store.Config.live_mode?() do
          nil
        else
          @data
        end
      end

      @doc """
      Returns the Stripe entity for `#{@mod_string}`.
      """
      def entity, do: @entity

      @doc """
      Returns the fields that can be used by `#{@mod_string}` to get records and entities.
      """
      def lookups do
        if Store.Config.zero_knowledge?() and @mod == Store.Customer do
          [:stripe_id]
        else
          if @lookups do
            @lookups
          else
            [:stripe_id]
          end
        end
      end

      @doc """
      Returns the `#{@mod_string}` main module.
      """
      def mod, do: @mod

      @doc """
      Returns `#{@mod_string}.Schema`.
      """
      def record, do: Module.concat(@mod, "Schema")

      @doc """
      Returns a string representation of `#{@mod_string}` for documentation.
      """
      def thing, do: String.downcase(@thing)

      @doc """
      Returns a string representation of `#{@mod_string}` for documentation.
      """
      def things, do: thing() <> "s"

      @doc """
      Returns a sample map that updates a `#{@mod_string}`.
      """
      def update_payload do
        @data.update_payload()
      end

      @doc """
      Returns `#{@mod_string}.Validator`.
      """
      def validator, do: Module.concat(@mod, "Validator")
    end
  end
end
