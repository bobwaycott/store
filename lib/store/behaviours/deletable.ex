defmodule Store.Deletable do
  @moduledoc false

  @doc false
  defmacro __using__(_opts) do
    quote location: :keep do
      alias Store.{Error, Repo, Util}
      # Module attributes
      @entity @meta.entity
      @mod @meta.mod
      @record @meta.record

      # Doc attributes
      @entity_string Util.docstring(@entity)
      @mod_string Util.docstring(@mod)
      @record_string Util.docstring(@record)

      @type delete_args :: integer | String.t()
      @type delete_result :: {:ok, @mod.t()} | Error.t()

      @doc """
      Deletes a `#{@meta.thing}` from database by primary key or optional identifier(s).

      Returns the deleted `#{@record_string}` record and `#{@entity_string}` entity.

      ### Optional identifier(s): `#{inspect(@meta.lookups)}`

      ## Examples

          iex> #{@mod_string}.delete(1)
          %#{@mod_string}{record: %#{@record_string}{}, entity: %#{@entity_string}{}}
      """
      @spec delete(delete_args) :: delete_result
      def delete(id) when is_integer(id) or is_binary(id) do
        with {:ok, thing} <- @mod.get(id) do
          case Repo.delete(thing.record) do
            {:ok, record} ->
              with {:ok, entity} <- @entity.delete(record.stripe_id) do
                @mod.cast(record, entity)
              else
                # In the rather unexpected event of a missing Stripe record?
                # This should only ever occur if a seeded record (that was not saved to Stripe) is deleted.
                err -> err
              end

            err ->
              err
          end
        else
          err -> err
        end
      end

      def delete(_), do: Error.invalid()

      @doc """
      Like `delete/1`, but raises database errors, if any.
      """
      @spec delete!(delete_args) :: delete_result
      def delete!(id) when is_integer(id) or is_binary(id) do
        thing = @mod.get!(id)
        record = Repo.delete!(thing.record)
        {:ok, entity} = @entity.delete(record.stripe_id)
        @mod.cast!(record, entity)
      end

      def delete!(_), do: Error.invalid()

      defoverridable delete: 1, delete!: 1
    end
  end
end
