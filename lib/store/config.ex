defmodule Store.Config do
  @moduledoc """
  Provides interaction with application configuration.

  Mainly used to allow certain features to be enabled/disabled.
  """

  @doc """
  Returns currencies configured for this `Store` instance.
  """
  @spec currencies() :: list
  def currencies do
    Application.get_env(:store, :currencies, [default_currency()])
  end

  @doc """
  Returns default currency configured for this `Store` instance.
  """
  @spec default_currency() :: String.t()
  def default_currency do
    Application.fetch_env!(:store, :default_currency)
  end

  @doc """
  Returns default locale configured for this `Store` instance.
  """
  @spec default_locale() :: String.t()
  def default_locale do
    Application.fetch_env!(:store, :default_locale)
  end

  @doc """
  Identify if `Store` is operating in production mode.
  """
  @spec live_mode?() :: boolean
  def live_mode? do
    Mix.env() == :prod
  end

  @doc """
  Returns locales configured for this `Store` instance.
  """
  @spec locales() :: list
  def locales do
    Application.get_env(:store, :locales, [default_locale()])
  end

  @doc """
  Identify if `Store` is operating in zero-knowledge mode.
  """
  @spec zero_knowledge?() :: boolean
  def zero_knowledge? do
    Application.fetch_env!(:store, :zero_knowledge)
  end
end
