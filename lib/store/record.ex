defmodule Store.Record do
  @moduledoc """
  Struct used throughout `Store` for all data.

  Contains a `record` (from database) and optional `entity` (from Stripe).
  """

  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts] do
      @entity Keyword.get(opts, :entity)
      @entity_string String.replace(Atom.to_string(@entity), "Elixir.", "")
      @mod_string String.replace(Atom.to_string(__MODULE__), "Elixir.", "")
      @record Keyword.get(opts, :record)
      @record_string String.replace(Atom.to_string(@record), "Elixir.", "")

      # Record type
      @type t :: %__MODULE__{entity: unquote(@entity).t() | nil, record: unquote(@record).t()}

      # typespecs
      @type ok :: {:ok, t()} | t()

      @enforce_keys :record
      defstruct [:entity, :record]

      @doc """
      Casts a `#{@record_string}` to a `#{@mod_string}` struct.

      ## Returned attributes

        - `:record` is the `#{@record_string}` record from database.

        - `:entity` **will always be `nil`**

      ## Examples

          iex> #{@mod_string}.cast(%#{@record_string}{})
          %#{@mod_string}{record: %#{@record_string}{}, entity: nil}
      """
      @spec cast(@record.t) :: ok
      def cast(record) when is_map(record) do
        {:ok, %__MODULE__{record: record, entity: nil}}
      end

      @doc """
      Like `cast/1`, but returns the `#{@mod_string}` struct directly.
      """
      @spec cast!(@record.t) :: ok
      def cast!(record) when is_map(record) do
        %__MODULE__{record: record, entity: nil}
      end

      @doc """
      Casts a `#{@record_string}` and `#{@entity_string}`to a `#{@mod_string}` struct.

      ## Returned attributes

        - `:record` is the `#{@record_string}` record from database.

        - `:entity` is the `#{@entity_string}` from Stripe API

      ## Examples

          iex> #{@mod_string}.cast(%#{@record_string}{}, %#{@entity_string}{})
          %#{@mod_string}{record: %#{@record_string}{}, entity: %#{@entity_string}{}}
      """
      @spec cast(@record.t, @entity.t) :: ok
      def cast(record, entity) when is_map(record) and is_map(entity) do
        {:ok, %__MODULE__{record: record, entity: entity}}
      end

      @doc """
      Like `cast/2`, but returns the `#{@mod_string}` struct directly.
      """
      @spec cast!(@record.t, @entity.t) :: ok
      def cast!(record, entity) when is_map(record) and is_map(entity) do
        %__MODULE__{record: record, entity: entity}
      end
    end
  end
end
