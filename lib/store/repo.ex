defmodule Store.Repo do
  use Ecto.Repo,
    otp_app: :store,
    adapter: Ecto.Adapters.Postgres

  def prefix do
    p =
      Application.get_env(:store, __MODULE__)
      |> Keyword.get(:prefix)

    case p do
      nil -> ""
      _ -> p
    end
  end
end
