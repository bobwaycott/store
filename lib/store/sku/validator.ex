defmodule Store.Sku.Validator do
  @moduledoc """
  Validator for `Store.Sku` structs.

  Used to validate data sent to create & update interfaces.
  """
  alias Ecto.Changeset
  alias Store.{Config, Currency, Util, Validators}

  @buckets ~w(in_stock limited out_of_stock)
  @entity %{
    active: :boolean,
    attributes: :map,
    currency: :string,
    image: :string,
    inventory: :map,
    metadata: :map,
    package_dimensions: :map,
    price: :integer,
    product: :integer
  }
  @required [:currency, :inventory, :price, :product]

  @doc """
  Validate data sent to create functions.
  """
  def validate_create(payload) do
    payload
    |> check_currency()
    |> convert_price()
    |> to_changeset()
    |> Changeset.validate_required(@required)
    |> Changeset.validate_number(:price, greater_than_or_equal_to: 0)
    |> Validators.validate_currency(:currency)
    |> Validators.validate_pkg(:height)
    |> Validators.validate_pkg(:length)
    |> Validators.validate_pkg(:weight)
    |> Validators.validate_pkg(:width)
    |> validate_inventory()
  end

  @doc """
  Validate data sent to update functions.
  """
  def validate_update(payload) do
    payload
    |> convert_price()
    |> to_changeset()
    |> Changeset.validate_number(:price, greater_than_or_equal_to: 0)
    |> Validators.validate_optional_currency(:currency)
    |> Validators.validate_pkg(:height)
    |> Validators.validate_pkg(:length)
    |> Validators.validate_pkg(:weight)
    |> Validators.validate_pkg(:width)
    |> validate_inventory()
  end

  # Maybe add currency if not provided & there is only 1 configured currency
  # If there are > 1 currencies configured, will not modify payload, so validation fails.
  defp check_currency(payload) do
    if is_nil(payload[:currency]) and length(Config.currencies()) == 1 do
      Map.put(payload, :currency, Currency.default())
    else
      payload
    end
  end

  # Ensure SKU price is always converted to proper smallest-unit amount
  # Will leave missing price to be caught by validation.
  defp convert_price(payload) do
    unless is_nil(payload[:price]) do
      %{payload | price: Currency.to_int(payload.price, payload[:currency])}
    else
      payload
    end
  end

  # Cast data to `Ecto.Changeset`.
  defp to_changeset(payload) do
    payload = Util.clean_strings(payload)

    {%{}, @entity}
    |> Changeset.cast(payload, Map.keys(@entity))
  end

  # Validates `inventory` map
  defp validate_inventory(changeset) do
    changes = changeset.changes

    unless is_nil(changes[:inventory]) do
      case Map.has_key?(changes.inventory, :type) do
        true ->
          case changes.inventory.type do
            "finite" ->
              validate_finite_inventory(changeset)

            "bucket" ->
              validate_bucket_inventory(changeset)

            "infinite" ->
              changeset

            _ ->
              changeset
              |> Changeset.add_error(
                :inventory,
                "invalid inventory :type -- must be \"bucket\", \"finite\", or \"infinite\""
              )
          end

        false ->
          changeset
          |> Changeset.add_error(:inventory, "inventory :type must be specified")
      end
    else
      changeset
    end
  end

  # Validates finite inventory includes quantity field
  defp validate_finite_inventory(changeset) do
    inventory = Map.get(changeset.changes, :inventory)

    case Map.has_key?(inventory, :quantity) do
      true ->
        case inventory.quantity >= 0 do
          true ->
            changeset

          _ ->
            changeset
            |> Changeset.add_error(
              :inventory,
              "finite inventory requires non-negative integer :quantity"
            )
        end

      false ->
        changeset
        |> Changeset.add_error(
          :inventory,
          "finite inventory requires non-negative integer :quantity"
        )
    end
  end

  # Validates bucket inventory includes value field
  defp validate_bucket_inventory(changeset) do
    inventory = Map.get(changeset.changes, :inventory)

    case Map.has_key?(inventory, :value) do
      true ->
        case inventory.value in @buckets do
          true ->
            changeset

          _ ->
            changeset
            |> Changeset.add_error(
              :inventory,
              "bucket inventory requires a :value of #{@buckets}"
            )
        end

      false ->
        changeset
        |> Changeset.add_error(
          :inventory,
          "bucket inventory requires a :value of #{@buckets}"
        )
    end
  end
end
