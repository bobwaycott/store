defmodule Store.Sku.Cast do
  @moduledoc """
  Casts inputs into proper outputs for Stripe API & DB use.
  """
  alias Store.Product
  alias Store.Sku.Meta
  alias Store.Util

  @record_keymap %{id: :stripe_id, product: :product_id}
  @record_keys [:active, :attributes, :currency, :image, :inventory, :price, :id, :product]
  @entity Meta.entity()
  @entity_keymap %{}
  @entity_keys [
    :active,
    :attributes,
    :currency,
    :image,
    :inventory,
    :metadata,
    :package_dimensions,
    :price,
    :product
  ]

  @doc """
  Returns `Map` for Stripe API insertion.
  """
  @spec entity(map) :: map
  def entity(%{} = data) do
    product = Product.get!(data.product)

    data
    |> Map.put(:product, product.record.stripe_id)
    |> Util.remap(@entity_keymap)
    |> Map.take(@entity_keys)
  end

  @doc """
  Returns `Map` for database insertion.
  """
  @spec record_create(@entity.t(), map) :: map
  def record_create(%@entity{} = entity, %{} = _) do
    product = Product.get!(entity.product)

    Map.take(entity, @record_keys)
    |> Util.remap(@record_keymap)
    |> Map.merge(%{product_id: product.record.id})
  end

  @doc """
  Returns `Map` for database updates.
  """
  @spec record_update(@entity.t()) :: map
  def record_update(%@entity{} = entity) do
    product = Product.get!(entity.product)

    entity
    |> Map.take(@record_keys)
    |> Util.remap(@record_keymap)
    |> Map.merge(%{product_id: product.record.id})
  end
end
