defmodule Store.Sku do
  @meta Store.Sku.Meta

  use Store.Interface
  use Store.Creatable
  use Store.Readable
  use Store.Updatable
  use Store.Deletable
end
