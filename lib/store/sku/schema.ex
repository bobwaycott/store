defmodule Store.Sku.Schema do
  @moduledoc """
  Schema definition for `Store.Sku` record.

  A `Store.Sku` is a stock-keeping&mdash;and price-providing&mdash;unit of a `Store.Product`.
  They are used for **things people can buy**. You'll attach SKUs to a `Store.Order` for
  e-commerce functionality.

  **NOTE:** *If you're looking to create subscription products/plans for your SAAS,
  check out `Store.Service`s, `Store.Subscription`s, and `Store.Plan`s.*

  `Store.Sku.Schema` provides the fields listed below. We believe these are likely to be the
  most commonly used attributes that would benefit from being available in your database,
  while other attributes/values can be retrieved by `Store.Sku.get_with_entity/1`.

  - `:active`&mdash;indicates if the SKU is active
  - `:attributes`&mdash;a map of up to 5 key-value pairs (e.g., `%{color: "blue", size: "medium"}`)
    - *Attribute keys must match the list of attributes defined on the associated `Store.Product`*
  - `:currency`&mdash;the 3-letter ISO currency code this SKU is priced in
  - `:id`&mdash;primary key of the database record
  - `:image`&mdash;a single image URL for this SKU, meant to be displayable to the customer
  - `:inventory`&mdash;a map describing the inventory `type` and `quantity` or `value`
  - `:price`&mdash;the price, in a `currency`'s smallest unit, of this SKU
  - `:stripe_id`&mdash;the unique Stripe identifier of the SKU

  ## Relationships

  A `Store.Sku` must be associated with a `Store.Product`.
  """
  use Ecto.Schema
  alias Ecto.Changeset
  alias Store.Product.Schema, as: Product

  @required [:currency, :inventory, :price, :stripe_id, :product_id]
  @optional [:active, :attributes, :image]

  schema "store_skus" do
    field(:active, :boolean, default: true)
    field(:attributes, :map)
    field(:currency, :string)
    field(:image, :string)
    field(:inventory, :map)
    field(:price, :integer)
    field(:stripe_id, :string)

    belongs_to(:product, Product)

    timestamps()
  end

  @doc """
  Returns a `Store.Sku.Schema` changeset.

  Will return an empty changeset without arguments.
  """
  def changeset(sku \\ %Store.Sku.Schema{}, attrs \\ %{}) do
    sku
    |> Changeset.cast(attrs, List.flatten(@required, @optional))
    |> Changeset.validate_required(@required)
    |> Changeset.unique_constraint(:stripe_id)
  end
end
