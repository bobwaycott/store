defmodule Store.Sku.Meta do
  use Store.Meta,
    data: Store.SkuFixture,
    entity: Stripe.Sku
end
