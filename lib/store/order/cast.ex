defmodule Store.Order.Cast do
  @moduledoc """
  Casts inputs into proper outputs for Stripe API & DB use.
  """
  alias Store.Util
  alias Store.Order.Meta

  @record_keymap %{id: :stripe_id}
  @record_keys []
  @entity Meta.entity()
  @entity_keymap %{}
  @entity_keys []

  @doc """
  Returns `Map` for database insertion.
  """
  @spec record_create(@entity.t(), map) :: map
  def record_create(%@entity{} = entity, %{} = data) do
    entity
    |> Map.take(@record_keys)
    |> Util.remap(@record_keymap)
    |> Map.merge(data)
  end

  @doc """
  Returns `Map` for database updates.
  """
  @spec record_update(@entity.t()) :: map
  def record_update(%@entity{} = entity) do
    entity
    |> Map.take(@record_keys)
    |> Util.remap(@record_keymap)
  end

  @doc """
  Returns `Map` for Stripe API insertion.
  """
  @spec entity(map) :: map
  def entity(%{} = data) do
    data
    |> Util.remap(@entity_keymap)
    |> Map.take(@entity_keys)
  end
end
