defmodule Store.Order.Schema do
  use Ecto.Schema
  alias Ecto.Changeset
  alias Store.Customer.Schema, as: Customer

  @required [:status, :stripe_id, :customer_id]

  schema "store_orders" do
    field(:status, :string)
    field(:stripe_id, :string)

    belongs_to(:customer, Customer)

    timestamps()
  end

  @doc """
  Returns a `Store.Order.Schema` changeset.

  Will return an empty changeset without arguments.
  """
  def changeset(order \\ %Store.Order.Schema{}, attrs \\ %{}) do
    order
    |> Changeset.cast(attrs, @required)
    |> Changeset.validate_required(@required)
    |> Changeset.unique_constraint(:stripe_id)
  end
end
