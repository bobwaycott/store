defmodule Store.Order.Meta do
  use Store.Meta,
    data: Store.OrderFixture,
    entity: Stripe.Order
end
