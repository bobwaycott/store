defmodule Store.Order do
  @meta Store.Order.Meta

  use Store.Interface
  use Store.Creatable
  use Store.Readable
  use Store.Updatable
end
