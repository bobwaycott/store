defmodule Store.Order.Validator do
  @moduledoc """
  Validator for `Store.Order` structs.

  Used to validate data sent to create & update interfaces.
  """
  alias Ecto.Changeset
  alias Store.Util

  @entity %{}
  @required []

  @doc """
  Validate data sent to create functions.
  """
  def validate_create(payload) do
    payload
    |> to_changeset()
  end

  @doc """
  Validate data sent to update functions.
  """
  def validate_update(payload) do
    payload
    |> to_changeset()
  end

  # Cast data to `Ecto.Changeset`.
  defp to_changeset(payload) do
    payload = Util.clean_strings(payload)

    {%{}, @entity}
    |> Changeset.cast(payload, Map.keys(@entity))
  end
end
