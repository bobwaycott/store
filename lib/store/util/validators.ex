defmodule Store.Validators do
  @moduledoc """
  Provides shared validation functions.
  """
  alias Ecto.Changeset

  @doc """
  Validates `:currency` is a valid ISO currency value.
  """
  @spec validate_currency(Changeset.t(), atom) :: Changeset.t()
  def validate_currency(changeset, key) do
    case Store.Currency.valid?(changeset.changes[key]) do
      true ->
        changeset

      false ->
        changeset
        |> Changeset.add_error(:currency, "must be declared")
    end
  end

  @doc """
  Validates `:currency` is a valid ISO currency value when present.
  """
  @spec validate_optional_currency(Changeset.t(), atom) :: Changeset.t()
  def validate_optional_currency(changeset, key) do
    unless is_nil(changeset.changes[key]) do
      case Store.Currency.valid?(changeset.changes[key]) do
        true ->
          changeset

        false ->
          changeset
          |> Changeset.add_error(:currency, "must be declared")
      end
    else
      changeset
    end
  end

  @doc """
  Validates `:package_dimensions` are valid for `Store.Product` & `Store.Sku`.
  """
  @spec validate_pkg(Changeset.t(), atom) :: Changeset.t()
  def validate_pkg(changeset, key) do
    unless is_nil(changeset.changes[:package_dimensions]) do
      case Map.has_key?(changeset.changes[:package_dimensions], key) do
        false ->
          changeset
          |> Changeset.add_error(:package_dimensions, "cannot be empty", key: key)

        true ->
          changeset
      end
    else
      changeset
    end
  end
end
