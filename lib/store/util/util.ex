defmodule Store.Util do
  @moduledoc """
  Various utilities for `Store`.
  """

  @doc """
  Clean strings in `attrs` map values that appear in `keys`.

  ## Examples

      iex> m = %{a: 876, b: [], c: "  at front", d: "at the end  ", e: "  both sides  "}
      iex> Util.clean_strings(m, [:c])
      %{a: 876, b: [], c: "at front", d: "at the end  ", e: "  both sides  "}
  """
  def clean_strings(attrs, keys) when is_map(attrs) and is_list(keys) and length(keys) > 0 do
    attrs
    |> strip_strings(keys)
    |> Enum.into(attrs)
  end

  @doc """
  Clean all string values in `attrs` map.

  ## Examples

      iex> m = %{a: 876, b: [], c: "  at front", d: "at the end  ", e: "  both sides  "}
      iex> Util.clean_strings(m)
      %{a: 876, b: [], c: "hey there", d: "at the end", e: "both sides"}
  """
  def clean_strings(attrs) when is_map(attrs) do
    attrs
    |> strip_strings()
    |> Enum.into(attrs)
  end

  @doc """
  Return a doc-ready string version of an atom.
  """
  def docstring(arg) when is_atom(arg) do
    arg
    |> Atom.to_string()
    |> String.replace("Elixir.", "")
  end

  @doc """
  Remaps `from` data into a new map by replacing keys with `keymap`.
  """
  def remap(from, keymap) do
    for {k, v} <- from, into: %{} do
      case keymap[k] do
        nil -> {k, v}
        key -> {key, v}
      end
    end
  end

  # Ensure we don't save strings with extra whitespace.
  # When `keys` is non-empty, we only strip the strings in `keys` list.
  defp strip_strings(attrs, keys) do
    Enum.map(attrs, fn {k, v} ->
      case k in keys and is_binary(v) do
        true -> {k, String.trim(v)}
        false -> {k, v}
      end
    end)
  end

  # Strip all strings inside `attrs` map.
  defp strip_strings(attrs) do
    Enum.map(attrs, fn {k, v} ->
      case is_binary(v) do
        true -> {k, String.trim(v)}
        false -> {k, v}
      end
    end)
  end
end
