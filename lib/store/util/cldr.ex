defmodule Store.Util.Cldr do
  @moduledoc false
  use Cldr,
    otp_app: :store,
    default_locale: Store.Config.default_locale(),
    locales: Store.Config.locales(),
    json_library: Jason,
    providers: [Cldr.Number],
    generate_docs: false
end
