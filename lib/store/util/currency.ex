defmodule Store.Currency do
  @moduledoc """
  Handles currency for a `Store` instance.

  `Store.Currency` can handle casting any Stripe-supported currency to/from smallest
  integer units, as well as formatting currencies as appropriate strings. It is very flexible,
  thanks to using `ex_money` under the hood. You can provide 3-letter ISO currency codes
  as strings or atoms.
  """
  alias Store.{Config, Error}

  @supported [:code.priv_dir(:store), "/currency/supported.txt"]
             |> :erlang.iolist_to_binary()
             |> File.read!()
             |> String.split()
             |> Enum.map(fn x -> String.to_atom(x) end)
             |> Enum.sort()
  @no_amex [:code.priv_dir(:store), "/currency/no_amex.txt"]
           |> :erlang.iolist_to_binary()
           |> File.read!()
           |> String.split()
           |> Enum.map(fn x -> String.to_atom(x) end)
           |> Enum.sort()
  @zero_decimal [:code.priv_dir(:store), "/currency/zero_decimal.txt"]
                |> :erlang.iolist_to_binary()
                |> File.read!()
                |> String.split()
                |> Enum.map(fn x -> String.to_atom(x) end)
                |> Enum.sort()

  @doc """
  Identify if `currency` can be used with American Express.

  ## Examples

      iex> Currency.can_use_amex? :USD
      true
      iex> Currency.can_use_amex? "usd"
      true
      iex> Currency.can_use_amex? :bob
      false
  """
  @spec can_use_amex?(atom | String.t()) :: boolean
  def can_use_amex?(currency) do
    with {:ok, code} <- validate!(currency) do
      code not in @no_amex
    else
      _ -> false
    end
  end

  @doc """
  Returns validated currency code as a string for consistent Stripe usage.

  ## Examples

      iex> Currency.code :usd
      "USD"
      iex> Currency.code "usd"
      "USD"
      iex> Currency.code :jpy
      "JPY"
      iex> Currency.code "JPY"
      "JPY"
  """
  @spec code(atom | String.t()) :: String.t()
  def code(currency \\ __MODULE__.default()) do
    with {:ok, code} <- validate!(currency) do
      Atom.to_string(code)
    end
  end

  @doc """
  Returns default currency for this store if valid currency.

  ## Examples

      iex> Currency.default
      :USD
  """
  @spec default() :: atom | Error.t()
  def default do
    with {:ok, code} <- validate!(Config.default_currency()) do
      code
    else
      _ -> Error.invalid_currency()
    end
  end

  @doc """
  Returns formatted `amount` as string in `currency` format.

  ## Examples

      iex> Currency.format 13.44
      "$13.44"
      iex> Currency.format "134.55"
      "$134.55"
      iex> Currency.format 100
      "$100.00"
      iex> Currency.format(100, :JPY)
      "¥100"
  """
  @spec format(number | String.t(), atom | String.t()) :: String.t()
  def format(amount, currency \\ __MODULE__.default()) do
    with {:ok, code} <- validate!(currency) do
      Money.new!(code, to_string(amount))
      |> Money.to_string!()
    end
  end

  @doc """
  Returns formatted `amount` as string in `currency` format.

  This should only be used when formatting smallest-unit amounts
  stored in Stripe entities.

  ## Examples

      iex> Currency.format_stripe 1000
      "$10.00"
      iex> Currency.format_stripe(1000, "usd")
      "$10.00"
      iex> Currency.format_stripe(1000, "JPY")
      "¥1,000"
      iex> Currency.format_stripe(12345, :JPY)
      "¥12,345"
  """
  @spec format_stripe(integer, atom | String.t()) :: String.t()
  def format_stripe(amount, currency \\ __MODULE__.default()) when is_integer(amount) do
    from_int(amount, currency)
    |> Money.to_string!()
  end

  @doc """
  Returns `amount` as `currency` in its standard form.

  ## Examples

      iex> Currency.from_int 1000
      Money.new(:USD, "10.00")
      iex> Currency.from_int 1000, :USD
      Money.new(:USD, "10.00")
      iex> Currency.from_int 1000, :jpy
      Money.new(:JPY, "1000")
  """
  @spec from_int(integer, atom | String.t()) :: Money.t() | Error.t()
  def from_int(amount, currency \\ __MODULE__.default()) when is_integer(amount) do
    with {:ok, code} <- validate!(currency) do
      Money.from_integer(amount, code)
    end
  end

  @doc """
  Returns list of currencies that do not support AMEX.

  ## Examples

      iex> Currency.no_amex
      [:AFN, :AOA, :ARS, :BOB, :BRL, :CLP, :COP, :CRC, :CVE, :CZK, :DJF, :FKP, :GNF,
      :GTQ, :HNL, :HUF, :INR, :LAK, :MUR, :NIO, :PAB, :PEN, :PYG, :SHP, :SRD, :UYU,
      :XOF, :XPF]
  """
  def no_amex, do: @no_amex

  @doc """
  Returns list of currencies that are supported by Stripe (and thus, by Store).

  ## Examples

      iex> Currency.supported
      [:AED, :AFN, :ALL, :AMD, :ANG, :AOA, :ARS, :AUD, :AWG, :AZN, :BAM, :BBD, :BDT,
      :BGN, :BIF, :BMD, :BND, :BOB, :BRL, :BSD, :BWP, :BZD, :CAD, :CDF, :CHF, :CLP,
      :CNY, :COP, :CRC, :CVE, :CZK, :DJF, :DKK, :DOP, :DZD, :EGP, :ETB, :EUR, :FJD,
      :FKP, :GBP, :GEL, :GIP, :GMD, :GNF, :GTQ, :GYD, :HKD, :HNL, :HRK, :HTG, :HUF,
      :IDR, :ILS, :INR, :ISK, :JMD, :JPY, :KES, :KGS, :KHR, :KMF, :KRW, :KYD, :KZT,
      :LAK, :LBP, :LKR, :LRD, :LSL, :MAD, :MDL, :MGA, :MKD, :MMK, :MNT, :MOP, :MRO,
      :MUR, :MVR, :MWK, :MXN, :MYR, :MZN, :NAD, :NGN, :NIO, :NOK, :NPR, :NZD, :PAB,
      :PEN, :PGK, :PHP, :PKR, :PLN, :PYG, :QAR, :RON, :RSD, :RUB, :RWF, :SAR, :SBD,
      :SCR, :SEK, :SGD, :SHP, :SLL, :SOS, :SRD, :STD, :SZL, :THB, :TJS, :TOP, :TRY,
      :TTD, :TWD, :TZS, :UAH, :UGX, :USD, :UYU, :UZS, :VND, :VUV, :WST, :XAF, :XCD,
      :XOF, :XPF, :YER, :ZAR, :ZMW]
  """
  def supported, do: @supported

  @doc """
  Returns proper amount to charge in `currency` as smallest-unit integer value.

  **NOTE:** We recommend supplying an amount as a properly formatted decimal or string
  for the `currency` you wish to have converted. This means, obviously, that you
  should pass the appropriate precision for **whole currency amounts** to avoid losing
  money in the integer conversion, or to avoid charging too much. For example, if your
  store is using `:USD` as its currency, you should send `amount` as dollars and cents,
  or whole dollars only (if there are no cents).

  ## Examples

      iex> Currency.to_int 234.45
      23445
      iex> Currency.to_int 200, "usd"
      20000
      iex> Currency.to_int "50.55", :usd
      5055
      iex> Currency.to_int 399.99, :EUR
      39999
      iex> Currency.to_int 23.456, :JPY
      23
  """
  @spec to_int(number | String.t(), atom | String.t() | nil) :: integer | Error.t()
  def to_int(amount, currency \\ __MODULE__.default()) do
    with {:ok, code} <- validate!(currency) do
      {_, int, _, _} =
        Money.new!(code, to_string(amount))
        |> Money.to_integer_exp()

      int
    end
  end

  @doc """
  Identify if `currency` is a supported currency.

  ## Examples

      iex> Currency.valid? "usd"
      true
      iex> Currency.valid? "USD"
      true
      iex> Currency.valid? :eur
      true
      iex> Currency.valid? :USD
      true
      iex> Currency.valid? "foo"
      false
  """
  @spec valid?(atom | String.t()) :: boolean
  def valid?(currency) do
    case validate!(currency) do
      {:ok, code} -> code in @supported
      {:error, _} -> false
    end
  end

  @doc """
  Returns list of zero-decimal currencies.

  ## Examples

      iex> Currency.zero_decimal
      [:BIF, :CLP, :DJF, :GNF, :JPY, :KMF, :KRW, :MGA, :PYG, :RWF, :UGX, :VND, :VUV,
      :XAF, :XOF, :XPF]
  """
  def zero_decimal, do: @zero_decimal

  # Get a valid currency code
  @spec validate!(atom | String.t()) :: atom | Error.t()
  defp validate!(currency) do
    case Money.validate_currency(currency) do
      {:ok, code} ->
        case code in @supported do
          true -> {:ok, code}
          false -> Error.invalid_currency()
        end

      _ ->
        Error.invalid_currency()
    end
  end
end
