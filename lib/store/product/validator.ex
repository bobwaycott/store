defmodule Store.Product.Validator do
  @moduledoc """
  Validator for `Store.Product` structs.

  Used to validate data sent to create & update interfaces.
  """
  alias Ecto.Changeset
  alias Store.Util
  import Store.Validators

  @entity %{
    active: :boolean,
    attributes: {:array, :string},
    caption: :string,
    deactivate_on: {:array, :string},
    description: :string,
    images: {:array, :string},
    name: :string,
    metadata: :map,
    package_dimensions: :map,
    shippable: :boolean,
    type: :string,
    url: :string
  }
  @required [:name, :type]

  @doc """
  Validate data sent to create functions.

  ## Validations

  - required fields are `#{inspect(@required)}`
  - product is always marked as `type: "good"`
  - `:attributes` does not exceed 5 items
  - `:images` does not exceed 8 items
  - if `:package_dimensions` is not `nil`:
    - `:height`, `:length`, `:weight`, and `:width` are present
  """
  def validate_create(payload) do
    payload
    |> Map.put(:active, true)
    |> Map.put(:type, "good")
    |> to_changeset()
    |> Changeset.validate_required(@required)
    |> Changeset.validate_length(:attributes, min: 0, max: 5)
    |> Changeset.validate_length(:images, min: 0, max: 8)
    |> validate_pkg(:height)
    |> validate_pkg(:length)
    |> validate_pkg(:weight)
    |> validate_pkg(:width)
  end

  @doc """
  Validate data sent to update functions.

  ## Validations

  - `:attributes` does not exceed 5 items
  - `:images` does not exceed 8 items
  - if `:package_dimensions` is not `nil`:
    - `:height`, `:length`, `:weight`, and `:width` are present
  """
  def validate_update(payload) do
    Enum.reject(payload, fn {k, _} -> k == :type end)
    |> Enum.into(%{})
    |> to_changeset()
    |> Changeset.validate_length(:attributes, min: 0, max: 5)
    |> Changeset.validate_length(:images, min: 0, max: 8)
    |> validate_pkg(:height)
    |> validate_pkg(:length)
    |> validate_pkg(:weight)
    |> validate_pkg(:width)
  end

  # Cast data to `Ecto.Changeset`.
  defp to_changeset(payload) do
    payload = Util.clean_strings(payload)

    {%{}, @entity}
    |> Changeset.cast(payload, Map.keys(@entity))
  end
end
