defmodule Store.Product.Cast do
  @moduledoc """
  Casts inputs into proper outputs for Stripe API & DB use.
  """
  alias Store.Util

  @record_keymap %{id: :stripe_id}
  @record_keys [:active, :attributes, :caption, :description, :id, :images, :name]

  # stripity-stripe always returns a `Stripe.Product` from API calls
  # We're going to have to use that for casts for now.
  @pre_entity Stripe.Product
  @entity Stripe.Relay.Product
  @entity_keys [
    :active,
    :attributes,
    :caption,
    :deactivate_on,
    :description,
    :images,
    :name,
    :metadata,
    :package_dimensions,
    :shippable,
    :type,
    :url
  ]

  @doc """
  Returns `Map` for Stripe API insertion.
  """
  @spec entity(map) :: map
  def entity(%{} = data) do
    data
    |> Map.take(@entity_keys)
  end

  @doc """
  Returns `Map` for database insertion.
  """
  @spec record_create(@pre_entity.t(), map) :: map
  def record_create(%@pre_entity{} = entity, _) do
    struct(@entity, Map.from_struct(entity))
    |> Map.take(@record_keys)
    |> Util.remap(@record_keymap)
  end

  @doc """
  Returns `Map` for database updates.
  """
  @spec record_update(@pre_entity.t()) :: map
  def record_update(%@pre_entity{} = entity) do
    struct(@entity, Map.from_struct(entity))
    |> Map.take(@record_keys)
    |> Util.remap(@record_keymap)
  end
end
