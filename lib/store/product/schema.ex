defmodule Store.Product.Schema do
  @moduledoc """
  Schema definition for a `Store.Product` record.

  A `Store.Product` is intended to be used for **things people can buy**.
  That means they'll be used with `Store.Sku`s and `Store.Order`s for e-commerce functionality.

  **NOTE:** *If you're looking to create subscription products/plans for your SAAS,
  check out `Store.Service`s, `Store.Subscription`s, and `Store.Plan`s.*

  ## Schema

  `Store.Product.Schema` provides the fields listed below. We believe these are likely to be the most commonly used
  attributes that would benefit from being available in your database, while other attributes/values can be
  retrived by `Store.Product.get_with_entity/1`.

  - `:active`&mdash;indicates if the product is active
  - `:attributes`&mdash;a list of up to 5 attributes that each `Store.Sku` can provide values for (e.g., `["color", "size"]`)
  - `:caption`&mdash;a short one-line description of the product, meant to be displayable to the customer
  - `:description`&mdash;the product’s description, meant to be displayable to the customer
  - `:id`&mdash;primary key of the database record
  - `:images`&mdash;a list of up to 8 URLs of images for the product, meant to be displayable to the customer
  - `:name`&mdash;the product’s name, meant to be displayable to the customer
  - `:stripe_id`&mdash;the unique Stripe identifier of the product

  ## Relationships

  A `Store.Product` can be associated with `Store.Sku`s.
  """
  use Ecto.Schema
  alias Ecto.Changeset
  alias Store.Sku.Schema, as: Sku

  @required [:name, :stripe_id]
  @optional [:active, :attributes, :caption, :description, :images]

  schema "store_products" do
    field(:active, :boolean, default: true)
    field(:attributes, {:array, :string})
    field(:caption, :string)
    field(:description, :string)
    field(:images, {:array, :string})
    field(:name, :string)
    field(:stripe_id, :string)

    has_many(:skus, Sku, foreign_key: :product_id)

    timestamps()
  end

  @doc """
  Returns a `Store.Product.Schema` changeset.

  Will return an empty changeset without arguments.
  """
  def changeset(product \\ %Store.Product.Schema{}, attrs \\ %{}) do
    product
    |> Changeset.cast(attrs, List.flatten(@required, @optional))
    |> Changeset.validate_required(@required)
    |> Changeset.validate_length(:attributes, min: 0, max: 5)
    |> Changeset.validate_length(:images, min: 0, max: 8)
    |> Changeset.unique_constraint(:stripe_id)
  end
end
