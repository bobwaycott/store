defmodule Store.Product.Meta do
  use Store.Meta,
    data: Store.ProductFixture,
    entity: Stripe.Relay.Product
end
