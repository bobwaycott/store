defmodule Store.Product do
  @meta Store.Product.Meta

  use Store.Interface
  use Store.Creatable
  use Store.Readable
  use Store.Updatable
  use Store.Deletable
end
