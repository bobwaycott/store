import Config

config :ex_unit,
  timeout: :infinity,
  exclude: [
    stripe_mock: true,
    stripe_api: true,
    no_zero: true
  ]

config :logger, :console,
  format: "[$level] $message\n",
  level: :error

# config :store, zero_knowledge: false

config :store, Store.Repo,
  database: "store_test",
  hostname: "localhost",
  pool_size: 10,
  # for assert_value during tests
  ownership_timeout: :infinity,
  timeout: :infinity

# import_config "test.secret.exs"
