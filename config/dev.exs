import Config

config :logger, :console, format: "[$level] $message\n"

config :store, Store.Repo,
  database: "store_dev",
  hostname: "localhost",
  pool_size: 10

# import_config "dev.secret.exs"
