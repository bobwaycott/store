import Config

config :ex_money,
  auto_start_exchange_rate_service: false,
  default_cldr_backend: Store.Util.Cldr,
  json_library: Jason

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :store,
  default_currency: :USD,
  default_locale: "en",
  ecto_repos: [Store.Repo],
  zero_knowledge: true

config :store, Store.Repo,
  database: "store",
  migration_source: "store_migrations"

config :stripity_stripe,
  json_library: Jason

# import environment-specific config
import_config "#{config_env()}.exs"
import_config "#{config_env()}.secret.exs"
